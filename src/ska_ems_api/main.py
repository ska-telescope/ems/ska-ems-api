"""
EMS API Gateway main file
"""

import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from ska_ems_api import config  # noqa: F401  # pylint: disable=W0611
from ska_ems_api.controllers import cmms, configuration_control, csdb, eda, lsa, prts
from ska_ems_api.services import auth

NAMESPACE_PREFIX = os.getenv("NAMESPACE_PREFIX", default="ska-ems-api")

app = FastAPI(
    title="Engineering Management System (EMS) API Gateway",
    summary="""
    This API is used to provide access to Engineering
    Management System (EMS) information
    """,
    docs_url=f"/{NAMESPACE_PREFIX}/docs",
    openapi_url=f"/{NAMESPACE_PREFIX}/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth.router)
app.include_router(configuration_control.router)
app.include_router(cmms.router)
app.include_router(prts.router)
app.include_router(csdb.router)
app.include_router(eda.router)
app.include_router(lsa.router)


@app.get("/")
async def root():
    """Unit test for the root path "/" """
    return {"message": "Hello World"}


@app.get("/test")
async def test():
    """Unit test for the root path "/" """
    db = os.getenv("POSTGRESS_DATABASE", default="No Env")
    return {"message": db}
