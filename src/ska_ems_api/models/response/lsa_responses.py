"""
Pydantic model representing the response schema for Logistic Support Analysis responses.
"""

from typing import Optional

from pydantic import BaseModel


class LSAResponse(BaseModel):
    """
    Pydantic model representing the response schema for a LSA PBS.

    Attributes:
      - system_name (str): The name of the system or subsystem.
      - lcn (Optional[str]): The Logistics Control Number (LCN) associated with the
        item.
      - lcn_name (str): The descriptive name of the item corresponding to the LCN.
      - qty_per_assembly (Optional[str]): The quantity of the item per assembly, if
        applicable.
      - lru (bool): Indicates whether the item is a Line Replaceable Unit (LRU).
      - part_number (Optional[str]): The part number of the item, if available.
      - cage_code (Optional[str]): The Commercial and Government Entity (CAGE) code
        associated with the supplier.
      - cage_name (Optional[str]): The name of the supplier or manufacturer.
    """

    system_name: str
    lcn: Optional[str] = None
    lcn_name: str
    qty_per_assembly: Optional[int] = None
    lru: bool
    part_number: Optional[str] = None
    cage_code: Optional[str] = None
    cage_name: Optional[str] = None
