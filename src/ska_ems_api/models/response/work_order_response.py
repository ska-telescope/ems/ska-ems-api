"""
Pydantic model representing the response schema for a work order.
"""

from typing import Optional

from pydantic import BaseModel


class WorkOrderResponse(BaseModel):
    """
    Pydantic model representing the response schema for a work order.

    Attributes:
      - task_id (int): The ID of the work order.
      - type (int): The type of the work order.
      - name (str): A brief summary of the work order.
      - description (str): Detailed description of the work order.
      - location_id (int): The ID of the location where the work order is to be carried
        out.
      - asset_id (Optional[int]): The ID of the asset associated with the work order, if
        any.
      - status (int): The status of the work order.
      - priority (int): The priority level of the work order.
      - start_date (Optional[int]): The start date of the work order, represented as a
        Unix timestamp.
      - due_date (Optional[int]): The due date of the work order, represented as a Unix
        timestamp.
      - created_date (int): The created date of the work order, represented as a Unix
        timestamp.
      - completed_date (int): The completed date of the work order, represented as a
        Unix timestamp.
      - last_edited_date (int): The last edited date of the work order, represented as a
        Unix timestamp.
    """

    task_id: int
    type: Optional[int] = None
    name: str
    description: str
    location_id: int
    asset_id: Optional[int] = None
    status: int
    priority: Optional[int] = None
    start_date: Optional[int] = None
    due_date: Optional[int] = None
    created_date: int
    completed_date: int
    last_edited_date: int
