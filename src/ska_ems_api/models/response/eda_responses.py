"""
Pydantic model representing the response schema for Engineering Data Archive (EDA)
responses.
"""

from pydantic import BaseModel


class ValueResponse(BaseModel):
    """
    Pydantic model representing the response schema for a EDA sensor value.

    Attributes:
        - name (str): The name of the sensor.
        - value (str): The value of the sensor.
        - date (float): The timestamp of the sensor value.
    """

    name: str
    value: str
    date: float
