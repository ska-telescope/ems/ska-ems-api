"""
Pydantic model representing the response schema for a ticket.
"""

from typing import List, Optional

from pydantic import BaseModel


class TicketComponentResponse(BaseModel):
    """
    Pydantic model representing the details of a component.

    Attributes:
        part_number (str): The part number extracted from the component's description.
        name (str): The name of the component.
    """

    part_number: str
    name: str


class TicketResponse(BaseModel):
    """
    Pydantic model representing the response schema for a ticket.

    Attributes:
      - id: The ID of the ticket.
      - key (str): The key of the ticket.
      - summary (str): A brief summary of the ticket.
      - description (Optional[str]): Description of the ticket.
      - components (Optional[List[Component]]): The components related to the ticket
        (optional).
      - status (Optional[str]): The current status of the ticket.
      - resolution (Optional[str]): The resolution of the ticket (optional).
      - created (str): The timestamp when the ticket was created.
      - resolutiondate (Optional[str]): The timestamp when the ticket was resolved.
    """

    id: int
    key: str
    summary: str
    description: Optional[str] = None
    components: Optional[List[TicketComponentResponse]] = None
    status: Optional[str] = None
    resolution: Optional[str] = None
    created_date: int
    resolution_date: Optional[int] = None
    url: str
