"""
Pydantic model representing the response schema for a data module.
"""

from typing import Optional

from pydantic import BaseModel


class DataModuleResponse(BaseModel):
    """
    Pydantic model representing the response schema for a data module.

    Attributes:
      - confluence_id (int): The ID of the confluence.
      - name (str): The name of the data module.
      - data_module_code (Optional[str]): The code of the data module.
      - issue_number (Optional[str]): The issue number associated with the data module.
      - part_number (Optional[str]): The part number associated with the data module.
      - url (str): The URL of the data module.
    """

    confluence_id: int
    name: str
    data_module_code: Optional[str] = None
    issue_number: Optional[str] = None
    part_number: Optional[str] = None
    url: str
