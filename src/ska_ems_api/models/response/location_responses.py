"""
Pydantic model representing the response schema for a work order.
"""

from typing import Optional

from pydantic import BaseModel


class RegionResponse(BaseModel):
    """
    Pydantic model representing the response schema for a region.
    """

    region_id: int
    name: str
    parent_region_id: Optional[int] = None


class LocationResponse(BaseModel):
    """
    Pydantic model representing the response schema for a location.
    """

    location_id: int
    name: str
    region_id: int
