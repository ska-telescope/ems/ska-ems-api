"""
Pydantic model representing the response schema for a work order.
"""

from typing import Optional

from pydantic import BaseModel


class AssetResponse(BaseModel):
    """
    Pydantic model representing the response schema for an asset.

    Attributes:
      - asset_id (int): Unique identifier for the asset.
      - name (str): Name of the asset.
      - parent_asset_id (int): Identifier for the parent asset.
      - location_id (int): Identifier for the location of the asset.
    """

    asset_id: int
    name: str
    parent_asset_id: int
    location_id: int


class AssetFieldResponse(BaseModel):
    """
    Pydantic model representing the response schema for an asset field.

    Attributes:
      - asset_id (int): The unique identifier for the asset.
      - field (str): The type of the asset field.
      - value (str): The value of the asset field.
      - type (str): The data type of the asset field.
      - last_edited_date (int): The timestamp of the last edit made to the asset field.
    """

    asset_id: int
    field: str
    value: Optional[str] = None
    type: str
    last_edited_date: int
