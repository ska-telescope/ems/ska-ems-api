"""
Pydantic model representing the response schema for a ticket work order.
"""

from pydantic import BaseModel


class TicketWorkOrderResponse(BaseModel):
    """
    Pydantic model representing the response schema for a ticket work order.

    Attributes:
      - id (int): The ID of the ticket.
      - url (str): The URL of the ticket.
      - title (str): The title of the ticket.
      - summary (str): A brief summary of the ticket.
      - resolved (bool): Whether the ticket is resolved or not.
    """

    id: int
    url: str
    title: str
    summary: str
    resolved: bool
