"""
Pydantic models for work orders
"""

from typing import Optional

from pydantic import BaseModel


class RemoveAndReplace(BaseModel):
    """
    A Pydantic model representing a removal and replacement of assets.

    Attributes:
        - installed_location_id (int): The ID of the location where the asset
          is installed.
        - installed_asset_id (int): The ID of the asset being removed and
          replaced.
        - warehouse_location_id (int): The ID of the warehouse location where
          the asset is stored.
        - warehouse_asset_id (int): The ID of the asset in the warehouse for
          replacement.
    """

    installed_location_id: int
    installed_asset_id: int
    warehouse_location_id: int
    warehouse_asset_id: int


class TicketWorkOrder(BaseModel):
    """
    A Pydantic model representing a ticket work order.

    Attributes:
        - id (int): The unique identifier of the ticket work order.
        - title (str): The title of the ticket work order.
        - summary (str): A brief summary of the ticket work order.
        - url (str): The URL associated with the ticket work order.
        - resolved (bool): A flag indicating whether the ticket work order has
          been resolved.
    """

    id: Optional[int] = None
    title: str
    summary: str
    url: str
    resolved: bool
