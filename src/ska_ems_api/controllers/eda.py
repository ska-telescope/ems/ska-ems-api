"""
Engineering Data Archive (EDA) endpoints
"""

from datetime import datetime
from typing import List

from fastapi import APIRouter, Depends

from ska_ems_api.models.response.eda_responses import ValueResponse
from ska_ems_api.services.auth import require_read_permission
from ska_ems_api.services.eda.interface import (
    get_att_current_values,
    get_att_value_ranges,
)

router = APIRouter()


# @router.get(
#     "/eda/value", tags=["Engineering Data Archive"], response_model=ValueResponse
# )
# def get_current_value(name: str):
#     """
#     Retrieve current value from the EDA
#     """
#     # name = """tango://databaseds.ska-low.svc
#     # .mccs.low.internal.skao.int:10000/low-mccs/smartbox/s8-1-sb07/status"""
#     value = get_att_current_value(name)

#     if value:
#         response = ValueResponse(
#             name=name,
#             value=str(value[0]),
#             date=value[1].timestamp(),
#         )
#         return response
#     return None


@router.post(
    "/eda/value",
    tags=["Engineering Data Archive"],
    response_model=List[ValueResponse],
    dependencies=[Depends(require_read_permission)],
)
def get_latest_values(names: List[str]):
    """
    Retrieve current values from the EDA for a list of names.

    Args:
        - names (List[str]): A list of attribute names to retrieve the current values
          for.

    Returns:
        List[ValueResponse]: A list of ValueResponse objects containing the current
        values of the attributes.
    """
    values = get_att_current_values(names)

    response = [
        ValueResponse(
            name=value[0],
            value=str(value[1]),
            date=value[2].timestamp(),
        )
        for value in values
    ]

    return response


@router.post(
    "/eda/value-range",
    tags=["Engineering Data Archive"],
    response_model=List[ValueResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_value_range(names: List[str], start_date: datetime, end_date: datetime):
    """
    Retrieve sensor value range from the EDA

    Args:
        - names (List[str]): List of sensor names to retrieve values for.
        - start_date (datetime): The start date for the value range.
        - end_date (datetime): The end date for the value range.

    Returns:
        List[ValueResponse]: A list of ValueResponse objects containing the sensor name,
        value, and timestamp.
    """
    values = get_att_value_ranges(names, start_date, end_date)

    response = [
        ValueResponse(
            name=value[0],
            value=str(value[1]),
            date=value[2].timestamp(),
        )
        for value in values
    ]
    return response
