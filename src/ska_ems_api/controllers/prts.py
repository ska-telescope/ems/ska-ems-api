"""
Problem Reporting and Tracking System endpoints
"""

from typing import List

from fastapi import APIRouter, Depends, Response
from fastapi.responses import JSONResponse

from ska_ems_api.models.response.ticket_response import TicketResponse
from ska_ems_api.models.response.ticket_work_order_response import (
    TicketWorkOrderResponse,
)
from ska_ems_api.models.shemas import TicketWorkOrder
from ska_ems_api.services.auth import require_read_permission, require_write_permission
from ska_ems_api.services.database import (
    create_issue_work_order_link,
    delete_issue_work_order_link,
)
from ska_ems_api.services.jira import jira_interface
from ska_ems_api.utils.helpers import convert_to_epoch

router = APIRouter()


@router.get(
    "/jira/tickets",
    tags=["Problem Reporting"],
    response_model=List[TicketResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_tickets():
    """
    Retrieve all tickets for the SPRTS project from JIRA and return a list of
    dictionaries containing key, summary, and description for each ticket.

    Returns:
        List[TicketResponse]: A list of TicketResponse objects containing ticket
        details.
    """
    rows = jira_interface.get_prts_tickets()

    response = []

    for row in rows["issues"]:
        fields = row.get("fields", {})

        resolution_name = (
            fields.get("resolution").get("name") if fields.get("resolution") else None
        )
        status = fields.get("status", {}).get("name")
        components = fields.get("components", [])
        component_details = []
        for component in components:
            description = component.get("description", "")
            if " - " in description:
                part_number, name = description.split(" - ", 1)
                component_details.append({"part_number": part_number, "name": name})

        created_date = convert_to_epoch(fields.get("created"))
        resolution_date = convert_to_epoch(fields.get("resolutiondate"))

        ticket = TicketResponse(
            id=row.get("id"),
            key=row.get("key"),
            summary=fields.get("summary"),
            description=fields.get("description"),
            components=component_details,
            status=status,
            resolution=resolution_name,
            created_date=created_date,
            resolution_date=resolution_date,
            url="https://jira.skatelescope.org/browse/" + row.get("key"),
        )
        response.append(ticket)

    return response


@router.get(
    "/jira/tickets/{key}/work-orders",
    tags=["Problem Reporting"],
    response_model=List[TicketWorkOrderResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_ticket_work_orders(key):
    """
    Retrieve the work orders remote link on a JIRA ticket.

    Args:
        - key (str): The key of the JIRA ticket.

    Returns:
        List[TicketWorkOrderResponse]: A list of TicketWorkOrderResponse objects
        containing work order details.
    """
    rows = jira_interface.get_ticket_work_order(key)

    # Convert the rows to a list of dictionaries
    response = []
    for row in rows:
        ticket = TicketWorkOrderResponse(
            id=row["id"],
            url=row["object"]["url"],
            title=row["object"]["title"],
            summary=row["object"]["summary"],
            resolved=row["object"]["status"]["resolved"],
        )
        response.append(ticket)

    return response


@router.put(
    "/jira/tickets/{key}/work-order",
    tags=["Problem Reporting"],
    dependencies=[Depends(require_write_permission)],
)
async def update_ticket_work_order(key, ticket_workorder: TicketWorkOrder):
    """
    Update the work order remote link for a specific JIRA ticket.

    Args:
        - key (str): The key of the JIRA ticket.
        - ticket_workorder (TicketWorkOrder): The work order details to update.

    Returns:
        Response: A response with status code 204.
    """
    jira_interface.update_ticket_work_order(key, ticket_workorder)

    return Response(status_code=204)


@router.post(
    "/jira/tickets/{key}/work-order",
    tags=["Problem Reporting"],
    dependencies=[Depends(require_write_permission)],
)
async def create_ticket_work_order(key, ticket_workorder: TicketWorkOrder):
    """
    Create a work order remote link in JIRA for a given issue key.

    Args:
        - key (str): The key of the JIRA ticket.
        - ticket_workorder (TicketWorkOrder): The work order details to create.

    Returns:
        None
    """
    title = ticket_workorder.title
    url = ticket_workorder.url
    summary = ticket_workorder.summary

    response = jira_interface.create_ticket_work_order(key, url, title, summary)

    # Store the link between the Jira issue and the CMMS task in database
    jira_remote_link_id = response["id"]
    task_id = ticket_workorder.title.split("#")[1]
    create_issue_work_order_link(key, jira_remote_link_id, task_id)

    return JSONResponse(
        status_code=201,
        content={
            "jira_remote_link_id": jira_remote_link_id,
        },
    )


@router.delete(
    "/jira/tickets/{key}/work-order",
    tags=["Problem Reporting"],
    dependencies=[Depends(require_write_permission)],
)
async def delete_ticket_work_order(key, link_id):
    """
    Delete a work order remote link from a JIRA ticket.

    Args:
        - key (str): The key of the JIRA ticket.
        - link_id (str): The identifier of the work order remote link to be deleted.

    Returns:
        Response: A response object with a status code of 204 indicating successful
        deletion.
    """

    jira_interface.delete_ticket_work_order(key, link_id)

    # Remove link between the Jira issue and the CMMS task in database
    delete_issue_work_order_link(key, link_id)

    return Response(status_code=204)
