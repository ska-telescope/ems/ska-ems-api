"""
Configuration Control endpoints
"""

import csv
from io import StringIO

from fastapi import APIRouter, HTTPException, Response

from ska_ems_api.services.alim import alim_interface

router = APIRouter()


@router.get(
    "/pbs",
    tags=["Configuration Control"],
    responses={
        500: {"description": "Failed to connect to the database"},
        200: {"description": "Successful Response"},
    },
)
async def get_product_breakdown_structure():
    """
    Returns a CSV file containing product breakdown structure of the SKA
    Observatory from an ALIM replica database.#

    Returns:
        Response: A CSV file containing the product breakdown structure.
    """
    rows = alim_interface.get_product_structure()
    if rows is None:
        raise HTTPException(status_code=500, detail="Failed to connect to the database")

    # Convert the rows to CSV format
    with StringIO() as csv_buffer:
        csv_writer = csv.writer(csv_buffer)
        csv_writer.writerow(
            [
                "id",
                "parent_id",
                "level",
                "item_number",
                "version",
                "description",
                "config_item",
                "seq_number",
                "uom",
                "status",
                "quantity_per",
                "item_class",
                "software",
                "serialised",
                "under_change",
                "main_equipment",
            ]
        )
        csv_writer.writerows(rows)
        csv_data = csv_buffer.getvalue().encode("utf-8")

    response = Response(content=csv_data, media_type="text/csv")
    response.headers["Content-Disposition"] = (
        "attachment; filename=product_structure.csv"
    )

    return response
