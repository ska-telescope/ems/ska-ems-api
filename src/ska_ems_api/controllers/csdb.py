"""
Technical Documentation endpoints
"""

import re
from typing import List

from fastapi import APIRouter, Depends

from ska_ems_api.models.response.data_module_responses import DataModuleResponse
from ska_ems_api.services.auth import require_read_permission
from ska_ems_api.services.confluence import api_interface
from ska_ems_api.utils.helpers import parse_html

router = APIRouter()


@router.get(
    "/data-modules",
    tags=["Technical Documentation"],
    response_model=List[DataModuleResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_data_modules():
    """
    Retrieve all data modules and return them as a list of DataModuleResponse objects.

    Returns:
        List[DataModuleResponse]: A list of DataModuleResponse objects containing
        information about each data module.
    """
    data_modules = api_interface.get_data_modules()

    response = []

    for data_module in data_modules.get("results", []):
        confluence_id = data_module.get("id")
        description = data_module.get("title", "No description available")
        url = (
            "https://confluence.skatelescope.org/rle/working-version/"
            + re.sub(r"[\s-]+", "-", data_module["title"].lower().strip())
            + "-"
            + data_module["id"]
            + ".html"
        )
        data_module_code = None
        part_number = None
        issue_number = None

        # Fetch and parse the confluence content
        confluence_content = api_interface.read_page_content(confluence_id)
        if confluence_content:
            data_module_code = confluence_content["title"].split(" - ")[0]
            html = confluence_content["body"]["storage"].get("value", "")
            datamodule_content = parse_html(html)

            if len(datamodule_content) >= 2:
                issue_number, part_number = datamodule_content[:2]

        response.append(
            DataModuleResponse(
                confluence_id=confluence_id,
                name=description,
                data_module_code=data_module_code,
                issue_number=issue_number,
                part_number=part_number,
                url=url,
            )
        )

    return response
