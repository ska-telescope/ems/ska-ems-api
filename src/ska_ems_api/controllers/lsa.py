"""
Logistic Support Analysis endpoints
"""

from typing import List

from fastapi import APIRouter, Depends

from ska_ems_api.models.response.lsa_responses import LSAResponse
from ska_ems_api.services.auth import require_read_permission
from ska_ems_api.services.ramlog import lsa_interface

router = APIRouter()


@router.get(
    "/lsa/pbs",
    tags=["Logistic Support Analysis"],
    response_model=List[LSAResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_product_breakdown_structure():
    """
    Retrieve Product Breakdown Structure from RamLog
    """
    product_breakdown_structure = lsa_interface.get_product_breakdown_structure()

    response = [
        LSAResponse(
            system_name=item[0],
            lcn=item[1],
            lcn_name=item[2],
            qty_per_assembly=item[3],
            lru=bool(item[4]),
            part_number=item[5],
            cage_code=item[6],
            cage_name=item[7],
        )
        for item in product_breakdown_structure
    ]

    return response
