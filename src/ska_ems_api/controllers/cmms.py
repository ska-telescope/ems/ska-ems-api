"""
Maintenance Management endpoints
"""

import os
from typing import List, Optional

import yaml
from fastapi import APIRouter, Depends, HTTPException, Path, Query
from fastapi.responses import JSONResponse

from ska_ems_api.models.response.asset_response import AssetFieldResponse, AssetResponse
from ska_ems_api.models.response.location_responses import (
    LocationResponse,
    RegionResponse,
)
from ska_ems_api.models.response.work_order_response import WorkOrderResponse
from ska_ems_api.services.auth import require_read_permission, require_write_permission
from ska_ems_api.services.cmms import (
    limble_asset,
    limble_asset_field_interface,
    limble_asset_interface,
    limble_location_interface,
    limble_task_interface,
)

router = APIRouter()


def load_config():
    """
    Load the configuration from the config.yaml file.

    Returns:
        dict: The configuration data.
    """
    # Get the absolute path to the config.yaml file
    config_path = os.path.join(os.path.dirname(__file__), "..", "config.yaml")

    # Open and load the YAML file
    with open(config_path, "r", encoding="utf-8") as config_file:
        config = yaml.safe_load(config_file)
    return config


@router.get(
    "/maintenance-schedule",
    tags=["Maintenance Management"],
    response_model=List[WorkOrderResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_maintenance_schedule(include_mid: bool = True, include_low: bool = True):
    """
    Retrieve the planned maintenance schedule from Limble.

    Args:
        - include_mid (bool): Whether to include mid locations in the schedule. Defaults
          to True.
        - include_low (bool): Whether to include low locations in the schedule. Defaults
          to True.

    Returns:
        list: A list of work order responses based on the selected locations.
    """
    config = load_config()
    all_mid_locations = config["locations"]["mid"]
    all_low_locations = config["locations"]["low"]

    # Combine locations if both are to be included
    selected_locations = []
    if include_mid:
        selected_locations.extend(all_mid_locations)
    if include_low:
        selected_locations.extend(all_low_locations)

    if not selected_locations:
        return []  # Return empty list if no locations are specified

    # Convert list of selected locations to a comma-separated string
    locations = ",".join(map(str, selected_locations))

    # Task types
    types = "1,4"

    # Fetch tasks based on selected locations
    tasks = limble_task_interface.get_tasks(
        locations=locations, types=types, statuses=0
    )

    # Process tasks into response format
    response = [
        WorkOrderResponse(
            task_id=task["taskID"],
            name=task["name"],
            description=task["description"],
            location_id=task["locationID"],
            asset_id=task["assetID"],
            status=task["status"],
            priority=task["priority"],
            start_date=task["startDate"],
            due_date=task["due"],
            created_date=task["createdDate"],
            date_completed=task["dateCompleted"],
        )
        for task in tasks
    ]

    return response


@router.get(
    "/limble/regions",
    tags=["Maintenance Management"],
    response_model=List[RegionResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_regions():
    """
    Retrieve regions from Limble.

    Returns:
        list: A list of regions.
    """
    regions = limble_location_interface.get_regions()

    response = [
        RegionResponse(
            region_id=region["regionID"],
            name=region["regionName"],
            parent_region_id=region["parentRegionID"],
        )
        for region in regions
    ]

    return response


@router.get(
    "/limble/locations",
    tags=["Maintenance Management"],
    response_model=List[LocationResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_locations():
    """
    Retrieve locations from Limble.

    Returns:
        list: A list of locations.
    """
    locations = limble_location_interface.get_locations()

    response = [
        LocationResponse(
            location_id=location["locationID"],
            name=location["name"],
            region_id=location["regionID"],
        )
        for location in locations
    ]

    return response


@router.get(
    "/limble/assets",
    tags=["Maintenance Management"],
    response_model=List[AssetResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_assets(asset_id: Optional[int] = None, location_id: Optional[int] = None):
    """
    Retrieve assets from Limble.

    Args:
        - asset_id (Optional[int]): The ID of the asset to retrieve. If None, retrieves
          all assets.
        - location_id (Optional[int]): The ID of the location to filter assets by. If
          None, retrieves assets from all locations.

    Returns:
        list: A list of assets.
    """
    assets = limble_asset_interface.get_assets(asset_id, location_id)

    response = [
        AssetResponse(
            asset_id=asset["assetID"],
            name=asset["name"],
            parent_asset_id=asset["parentAssetID"],
            location_id=asset["locationID"],
        )
        for asset in assets
    ]

    return response


@router.patch("/limble/assets/{asset_id}/replace", tags=["Maintenance Management"])
async def remove_replace_asset(
    asset_id: int = Path(..., description="The ID of the asset to be replaced"),
    location_id: int = Query(
        ..., description="The ID of the location where the asset will be moved"
    ),
    location_parent_asset_id: Optional[int] = Query(
        None, description="The ID of the parent asset for the location"
    ),
    spare_id: Optional[int] = Query(
        None,
        description="The ID of the spare asset to replace the original asset",
    ),
):
    """
    Replace an asset with a spare asset in Limble and move the original asset to a new
    location.

    Args:
        - asset_id (int): The ID of the asset to be replaced.
        - location_id (int): The ID of the location where the asset will be moved.
        - location_parent_asset_id (Optional[int]): The ID of the parent asset for the
          location.
        - spare_id (Optional[int]): The ID of the spare asset to replace the original
          asset.

    Raises:
        HTTPException: If the asset or spare asset does not exist.

    Returns:
        JSONResponse: A response indicating the asset has been moved.
    """
    # Asset information
    asset = limble_asset_interface.get_assets(asset_id)
    if not asset:
        raise HTTPException(status_code=404, detail="Asset does not exist")

    asset_parent_id = asset[0]["parentAssetID"]
    asset_location_id = asset[0]["locationID"]
    asset_name = asset[0]["name"]

    # Spare information
    if spare_id is None:
        # Create placeholder
        spare_id = limble_asset.create_placeholder_asset(asset_id, asset_location_id)
        spare = limble_asset_interface.get_assets(spare_id)
    else:
        spare = limble_asset_interface.get_assets(spare_id)
        if not spare:
            raise HTTPException(status_code=404, detail="Spare asset does not exist")

        # Move spare to asset location
        limble_asset.move_asset_with_children(spare_id, asset_location_id)

    spare_name = spare[0]["name"]

    # Update spare parent and name
    limble_asset_interface.update_asset(
        spare_id, parent_asset_id=asset_parent_id, name=asset_name
    )

    # Swap the topological names when the asset and spare asset names are the same
    limble_asset.update_topological_names(asset_id, spare_id)

    # Remove asset from the slot
    limble_asset.move_asset_with_children(asset_id, location_id)
    limble_asset_interface.update_asset(
        asset_id, parent_asset_id=location_parent_asset_id, name=spare_name
    )

    response = {"moved": True}
    return JSONResponse(status_code=201, content=response)


@router.patch(
    "/limble/assets/{asset_id}/move",
    tags=["Maintenance Management"],
)
async def move_asset(asset_id: int, location_id: int):
    """
    Move an asset and its children to a new location in Limble.

    Args:
        - asset_id (int): The ID of the asset to be moved.
        - location_id (int): The ID of the new location where the asset will be moved.

    Returns:
        dict: A dictionary containing the result of the move operation with a key
        'moved' indicating success.

    Raises:
        HTTPException: If the asset does not exist, an HTTP 404 error is raised.
    """

    asset = limble_asset_interface.get_assets(asset_id)
    if not asset:
        raise HTTPException(status_code=404, detail="Asset does not exist")

    moved = limble_asset.move_asset_with_children(asset_id, location_id)

    response = {"moved": moved}
    return response


@router.get(
    "/limble/asset_fields",
    tags=["Maintenance Management"],
    response_model=List[AssetFieldResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_asset_fields():
    """
    Retrieve asset fields and values from Limble.

    Returns:
        list: A list of asset fields and values.
    """
    fields = [
        "Alim Part Number",
        "Alim Version",
        "Serial Number",
        "Topological Name",
    ]

    asset_field_values = []
    for field in fields:
        asset_field_values.extend(
            limble_asset_field_interface.get_asset_fields(name=field)
        )

    response = [
        AssetFieldResponse(
            asset_id=asset_field["assetID"],
            field=asset_field["field"],
            value=asset_field["value"],
            type=asset_field["fieldType"],
            last_edited_date=asset_field["lastEdited"],
        )
        for asset_field in asset_field_values
    ]

    return response


@router.get(
    "/limble/tasks",
    tags=["Maintenance Management"],
    response_model=List[WorkOrderResponse],
    dependencies=[Depends(require_read_permission)],
)
async def get_tasks(task_id: Optional[int] = None, asset_id: Optional[int] = None):
    """
    Retrieve work orders based on optional parameters from Limble.

    Args:
        - task_id (Optional[int]): The ID of the task to retrieve. Defaults to None.
        - asset_id (Optional[int]): The ID of the asset to retrieve tasks for. Defaults
          to None.

    Returns:
        list: A list of work order responses based on the provided parameters.
    """
    tasks = limble_task_interface.get_tasks(task_id, asset_id)

    response = [
        WorkOrderResponse(
            task_id=task["taskID"],
            type=task["type"] if task["type"] != "Unknown" else None,
            name=task["name"],
            description=task["description"],
            location_id=task["locationID"],
            asset_id=task["assetID"],
            status=task["status"],
            priority=task["priority"],
            start_date=task["startDate"],
            due_date=task["due"],
            created_date=task["createdDate"],
            completed_date=task["dateCompleted"],
            last_edited_date=task["lastEdited"],
        )
        for task in tasks
    ]

    return response


@router.post(
    "/limble/tasks",
    tags=["Maintenance Management"],
    dependencies=[Depends(require_write_permission)],
)
async def create_task(work_order: WorkOrderResponse):
    """
    Create a work order in Limble.

    Args:
        - work_order (WorkOrderResponse): The work order details including name,
          description, and location_id.
    Returns:
        JSONResponse: A JSON response with status code 201 and the content of the
        created task.
    """
    name = work_order.name
    description = work_order.description
    location_id = work_order.location_id
    response = limble_task_interface.create_task(name, description, location_id)

    return JSONResponse(status_code=201, content=response)


@router.post(
    "/limble/tasks/{task_id}/comment",
    tags=["Maintenance Management"],
    dependencies=[Depends(require_write_permission)],
)
async def create_task_comment(task_id: int, comment: str):
    """
    Create a task comment in Limble.

    Args:
        - task_id (int): The ID of the task to which the comment will be added.
        - comment (str): The content of the comment to be added.
    Returns:
        JSONResponse: A response object containing the status code and the response
        content from the Limble API.
    """
    response = limble_task_interface.create_task_comment(
        task_id,
        comment,
    )

    return JSONResponse(status_code=201, content=response)
