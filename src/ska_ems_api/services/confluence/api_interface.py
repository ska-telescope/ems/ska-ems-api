"""
Confluence API interface functions
"""

import os

from ska_ems_api.utils.api_requests import get_request
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

CONFLUENCE_TOKEN = os.getenv("CONFLUENCE_TOKEN", "")
CONFLUENCE_API_URL = os.getenv("CONFLUENCE_API_URL")

headers = {"Authorization": "Bearer " + CONFLUENCE_TOKEN}


@handle_request_exceptions
def get_data_modules():
    """
    Retrieve all data modules for the RLE space in Confluence.

    Returns:
        - dict: A JSON object containing the details of all data modules.
    """
    # 138651251 - OBP
    # 127500685 - LOW
    # 227151264 - MID
    # 227149561 - COM

    api_url = f"{CONFLUENCE_API_URL}/content/search"

    params = {
        "cql": """(space=RLE and type=page and ancestor IN
        (138651251,127500685,227151264,227149561)
        and title ~ "DMC%")""",
        "limit": 99999,
    }

    request = get_request(api_url, params, headers)
    response = request.make_request()

    messages = {"success": "Successfully retrieved all data modules from Confluence"}
    return handle_response(response, messages, return_json=True)


@handle_request_exceptions
def read_page_content(page_id):
    """
    Retrieve the content of a Confluence page.

    Args:
        - page_id (str): The ID of the page to retrieve content for.

    Returns:
        - dict: A dictionary containing the asset fields retrieved from the API
    """
    api_url = f"{CONFLUENCE_API_URL}/content/{page_id}"
    params = {
        "expand": "body.storage",
    }

    request = get_request(api_url, params, headers)
    response = request.make_request()

    messages = {"success": f"Successfully retrieved page content for page ({page_id})"}
    return handle_response(response, messages, return_json=True)
