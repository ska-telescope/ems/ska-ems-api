"""
LIMBLE API interface for asset functions
"""

import json

from ska_ems_api.utils.api_requests import (
    LIMBLE_BASEURL,
    limble_delete_request,
    limble_get_request,
    limble_patch_request,
    limble_post_request,
    limble_put_request,
)
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


@handle_request_exceptions
def get_asset_fields(asset_id=None, name=None, location_id=None, start=None):
    """
    Retrieve asset fields for a given asset ID from Limble.

    Args:
        - asset_id (str): This parameter can be used to get a single Asset or a list of
          Assets in a comma-separated list.
        - name (str): This parameter is used to only get specific field by name. This
          parameter expects a string full name of a field or partial name with the
          wildcard %.
        - start (int): This parameter is used filter Asset Fields for Assets that were
          last edited after the unix timestamp passed into the start parameter

    Returns:
        - dict: A dictionary containing the asset fields retrieved from the API.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/fields"
    params = {"limit": 99999}

    identifiers = {}
    if asset_id:
        params["assets"] = asset_id
        identifiers["asset_id"] = asset_id
    if name:
        params["name"] = name
        identifiers["name"] = name
    if location_id:
        params["locations"] = location_id
    if start:
        params["start"] = start

    request = limble_get_request(api_url, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved asset fields for (%s)"}
    return handle_response(
        response,
        messages,
        identifiers=identifiers,
        return_json=True,
    )


@handle_request_exceptions
def get_suggested_field(location_id=None, name=None):
    """
    Retrieve suggested field ID for a given location and field name.

    Args:
        - location_id (str): The ID of the location for which to retrieve suggested
          field ID.
        - name (str): The name of the field for which to retrieve suggested field ID.

    Returns:
        - dict: A dictionary containing the suggested field ID information if
          successful.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/fields/suggested"
    params = {"limit": 9999}

    if location_id:
        params["locations"] = location_id
    if name:
        params["name"] = name

    request = limble_get_request(api_url, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved asset fields for location (%s)"}
    return handle_response(
        response, messages, identifiers={"location_id": location_id}, return_json=True
    )


@handle_request_exceptions
def create_suggested_field_field(location_id, name):
    """
    Create a new location field for a given location ID with the specified name in
    Limble.

    Args:
        - location_id (int): The ID of the location for which the field is being
          created.
        - name (str): The name of the location field to be created.

    Returns:
        - dict: If the location field is success fully created, the JSON response of the
          created field.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/fields/"
    payload = json.dumps({"locationID": {location_id}, "name": name, "fieldType": 1})

    request = limble_post_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully created location field %s",
        "error": "Failed to create location field.",
    }
    return handle_response(
        response,
        messages,
        identifiers={"location_id": location_id},
        status_code=201,
        return_json=True,
    )


@handle_request_exceptions
def update_asset_field_value(value_id, value):
    """
    Update a specific field value of an asset in Limble.

    Args:
        - value_id (str): The ID of the value that needs to be updated.
        - value (str): The new value to be set for the field.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/fields/{value_id}/"
    payload = json.dumps({"value": value})

    request = limble_patch_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully updated asset field value (%s)",
        "error": "Failed to update asset fields.",
    }
    handle_response(response, messages, identifiers={"value_id": value_id})


@handle_request_exceptions
def link_asset_field(asset_id, location_id, field_id):
    """
    Link a field to an asset in Limble.

    Args:
        - asset_id (str): The ID of the asset to link the field to.
        - location_id (str): The ID of the location where the field is linked.
        - field_id (str): The ID of the field to be linked to the asset.

    Returns:
        - dict: A JSON response containing the result of the linking operation.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/{asset_id}/fields"
    payload = json.dumps({"locationID": location_id, "fieldID": field_id})

    request = limble_put_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully linked field(%s) to asset(%s)",
        "error": "Failed to create asset field link.",
    }
    return handle_response(
        response,
        messages,
        identifiers={"asset_id": asset_id, "field_id": field_id},
        return_json=True,
    )


@handle_request_exceptions
def delete_asset_field(field_id):
    """
    Delete a specific field of an asset in Limble.

    Args:
        - asset_id (str): The ID of the asset field that needs to be deleted.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/fields/{field_id}/"

    request = limble_delete_request(api_url)
    response = request.make_request()

    messages = {
        "success": "Successfully deleted asset field %s",
        "error": "Failed to delete asset field.",
    }
    handle_response(response, messages, identifiers={"field_id": field_id})
