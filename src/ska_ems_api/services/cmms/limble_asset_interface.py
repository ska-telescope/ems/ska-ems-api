"""
LIMBLE API interface for asset functions
"""

import json

import requests
from requests.auth import HTTPBasicAuth

from ska_ems_api.utils.api_requests import (
    LIMBLE_BASEURL,
    LIMBLE_PASSWORD,
    LIMBLE_USERNAME,
    limble_get_request,
    limble_patch_request,
    limble_post_request,
)
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


@handle_request_exceptions
def get_assets(asset_id=None, location_id=None):
    """
    Get assets based on optional asset_id and location_id from Limble.

    Args:
        - asset_id (str, optional): The ID of the asset to filter by. Defaults to None.
        - location_id (str, optional): The ID of the location to filter by. Defaults to
          None.

    Returns:
        - dict or None: A dictionary containing the assets information if successful,
          None otherwise.
    """
    api_url = f"{LIMBLE_BASEURL}/assets"
    params = {"limit": 9999}

    if asset_id:
        params["assets"] = asset_id

    if location_id:
        params["locations"] = location_id

    request = limble_get_request(api_url, params)
    response = request.make_request()

    # Update the messages dictionary with different success messages
    if asset_id and location_id:
        messages = {
            "success": "Successfully retrieved assets for asset (%s) and location (%s)"
        }
        identifiers = {"asset_id": asset_id, "location_id": location_id}
    elif location_id:
        messages = {"success": "Successfully retrieved assets for location (%s)"}
        identifiers = {"location_id": location_id}
    else:
        messages = {"success": "Successfully retrieved assets (%s)"}
        identifiers = {"asset_id": asset_id}
    return handle_response(
        response,
        messages,
        identifiers,
        return_json=True,
    )


@handle_request_exceptions
def get_asset_children(asset_id):
    """
    Get the children assets of a given asset ID from Limble.

    Args:
        - asset_id (str): The ID of the asset for which to retrieve children assets.

    Returns:
        - dict or None: A dictionary containing the JSON response from the API if the
          request is successful (status code 200), None if the request is unauthorized
          (status code 401), or None if an unexpected response is received.
    """
    api_url = f"{LIMBLE_BASEURL}/assets"
    params = {"limit": 9999}

    if asset_id:
        params["parentAssetID"] = asset_id

    request = limble_get_request(api_url, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved asset children for asset (%s)"}
    return handle_response(
        response, messages, identifiers={"asset_id": asset_id}, return_json=True
    )


@handle_request_exceptions
def create_asset(name, location_id, parent_asset_id=None):
    """
    Create an asset in Limble.

    Args:
        - name (str): The name of the asset.
        - location_id (str): The ID of the location where the asset is located.
        - parent_asset_id (str, optional): The ID of the parent asset, if the asset has
          one. Defaults to None.

    Returns:
        - dict or None: If the asset is successfully created, the function returns a
          dictionary containing the asset information. If there is an error, the
          function returns None.
    """
    api_url = f"{LIMBLE_BASEURL}/assets"
    payload = {"name": name, "locationID": location_id}

    if parent_asset_id:
        payload["parentAssetID"] = parent_asset_id

    payload = json.dumps(payload)
    request = limble_post_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully created asset (%s)",
        "error": "Failed to create asset.",
    }
    return handle_response(
        response,
        messages,
        identifiers={"name": name},
        status_code=201,
        return_json=True,
    )


@handle_request_exceptions
def delete_asset(asset_id):
    """
    Deletes an asset from Limble.

    Args:
        - asset_id (str): The ID of the asset to be deleted.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/{asset_id}/"

    response = requests.delete(
        api_url,
        auth=HTTPBasicAuth(LIMBLE_USERNAME, LIMBLE_PASSWORD),
        timeout=10,
    )

    messages = {
        "success": "Successfully deleted asset %s",
        "error": "Failed to delete asset.",
    }
    return handle_response(
        response, messages, identifiers={"asset_id": asset_id}, status_code=201
    )


@handle_request_exceptions
def update_asset(asset_id, parent_asset_id=None, name=None, started_on=None):
    """
    Updates an asset's top level information in the Limble. See update_asset_field_value
    to update an Asset Field's value.

    Args:
        - asset_id (int): The ID of the asset to be updated.
        - parent_asset_id (int, optional): The ID of the parent asset. Defaults to None.
        - name (str, optional): The name of the asset. Defaults to None.
        - started_on (str, optional): The start date of the asset. Defaults to None.

    Returns:
        None
    """
    api_url = f"{LIMBLE_BASEURL}/assets/{asset_id}/"
    payload = {}

    if parent_asset_id:
        payload["parentAssetID"] = parent_asset_id

    if name:
        payload["name"] = name

    if started_on:
        payload["startedOn"] = started_on

    payload = json.dumps(payload)

    request = limble_patch_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully updated top level asset information for asset (%s)",
        "error": "Failed to update top level asset information.",
    }
    handle_response(response, messages, identifiers={"asset_id": asset_id})


@handle_request_exceptions
def move_asset(asset_id, location_id):
    """
    Moves an asset to a specified location in Limble.

    Args:
        - asset_id (str): The ID of the asset to be moved.
        - location_id (str): The ID of the location to which the asset should be moved.
    """
    api_url = f"{LIMBLE_BASEURL}/assets/{asset_id}/move"
    payload = json.dumps({"locationID": location_id})

    request = limble_patch_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully moved asset (%s) to location (%s)",
        "error": "Failed to move asset.",
    }
    handle_response(
        response,
        messages,
        identifiers={"asset_id": asset_id, "location_id": location_id},
    )
