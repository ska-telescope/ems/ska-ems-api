"""
LIMBLE API interface for location functions
"""

from ska_ems_api.utils.api_requests import LIMBLE_BASEURL, limble_get_request
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


@handle_request_exceptions
def get_locations():
    """
    Retrieve locations from Limble.

    Returns:
        dict: A JSON object containing the locations if successful, None
        otherwise.
    """
    api_url = f"{LIMBLE_BASEURL}/locations"
    params = {"limit": 9999}

    request = limble_get_request(api_url, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved locations"}
    return handle_response(response, messages, return_json=True)


@handle_request_exceptions
def get_regions():
    """
    Retrieve regions from Limble.

    Returns:
        dict: A JSON object containing the locations if successful, None
        otherwise.
    """
    api_url = f"{LIMBLE_BASEURL}/regions"
    params = {"limit": 9999}

    request = limble_get_request(api_url, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved regions"}
    return handle_response(response, messages, return_json=True)
