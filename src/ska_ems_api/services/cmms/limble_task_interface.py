"""
LIMBLE API interface for task functions
"""

import json
import time
from typing import Optional

from ska_ems_api.utils.api_requests import (
    LIMBLE_BASEURL,
    limble_get_request,
    limble_patch_request,
    limble_post_request,
)
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


@handle_request_exceptions
def get_tasks(
    task_id: Optional[int] = None,
    asset_id: Optional[int] = None,
    locations: Optional[str] = None,
    types: Optional[str] = None,
    statuses: Optional[str] = None,
):
    """
    Retrieve tasks based on optional parameters from Limble.

    Args:
        - task_id (Optional[int]): The ID of the specific task to retrieve.
        - asset_id (Optional[int]): The ID of the asset associated with the tasks to
          retrieve.
        - locations (Optional[str]): A comma-separated list of location identifiers to
          filter tasks by location.
        - types (Optional[str]): A comma-separated list of task types to filter the
          results.
        - statuses (Optional[str]): A comma-separated list of task statuses to filter
          the results.

    Returns:
        - dict: A JSON object containing the retrieved tasks.
    """
    api_url = f"{LIMBLE_BASEURL}/tasks"
    params = {"limit": 9999}

    if task_id:
        params["tasks"] = task_id
    if asset_id:
        params["assets"] = asset_id
    if locations:
        params["locations"] = locations
    if types:
        params["type"] = types
    if statuses:
        params["status"] = statuses

    request = limble_get_request(api_url, params)

    response = request.make_request()
    return handle_response(response, return_json=True)


@handle_request_exceptions
def create_task(name, description, location_id):
    """
    Create a task in Limble.

    Args:
        - name (str): The name of the task to be created.
        - description (str): The description of the task.
        - locationID (int): The ID of the location where the task will be
          assigned.

    Returns:
        - dict: A dictionary containing the details of the created task if
          successful.
    """
    api_url = f"{LIMBLE_BASEURL}/tasks"
    payload = json.dumps(
        {
            "name": name,
            "locationID": location_id,
            "due": time.time(),
            "type": 6,
            "description": description,
        }
    )

    request = limble_post_request(api_url, payload)
    response = request.make_request()

    messages = {"error": "Failed to create task."}
    return handle_response(response, messages, status_code=201, return_json=True)


@handle_request_exceptions
def create_task_comment(task_id, comment):
    """
    Create a comment for a specific task in Limble.

    Args:
        - taskID (str): The ID of the task for which the comment is being
          created.
        - comment (str): The text content of the comment to be added to the
          task.

    Returns:
        - dict: If the comment is successfully created, returns the JSON
          response containing the created comment.
    """
    api_url = f"{LIMBLE_BASEURL}/tasks/{task_id}/comments"
    payload = json.dumps({"comment": comment, "showExternalUsers": True})

    request = limble_post_request(api_url, payload)
    response = request.make_request()

    messages = {"error": "Failed to create task comment."}
    return handle_response(response, messages, status_code=201, return_json=True)


@handle_request_exceptions
def update_task_asset(task_id, asset_id):
    """
    Update the asset associated with a specific task in Limble.

    Args:
        - taskID (str): The ID of the task to update.
        - assetID (str): The ID of the asset to associate with the task.
    """
    api_url = f"{LIMBLE_BASEURL}/tasks/{task_id}"
    payload = json.dumps({"assetID": asset_id})

    request = limble_patch_request(api_url, payload)
    response = request.make_request()

    messages = {
        "success": "Successfully updated task %s asset to %s",
        "error": "Failed to update task asset.",
    }
    return handle_response(
        response,
        messages,
        identifiers={"task_id": task_id, "asset_id": asset_id},
        return_json=True,
    )
