"""
LIMBLE API for asset functions
"""

from ska_ems_api.services.cmms import (
    limble_asset_field_interface,
    limble_asset_interface,
)
from ska_ems_api.services.database import get_physical_item_name


def move_asset_with_children(asset_id, location_id):
    """
    Move an asset and its children to a specified location in Limble.

    Args:
        - asset_id (str): The ID of the asset to be moved along with its children.
        - location_id (str): The ID of the location to which the asset and its children
          should be moved.

    Returns:
        - bool: True if the assets are successfully moved and updated, False otherwise.
    """

    # Recursively get all assets including the parent and its children
    asset_with_children = get_all_child_assets(asset_id)

    # Iterate over the assets from the bottom up
    for child_asset in reversed(asset_with_children):
        asset_id = child_asset["assetID"]
        limble_asset_interface.move_asset(asset_id, location_id)

    # Update root asset to the hierarchical position in the location
    asset_with_children[0]["parentAssetID"] = 0

    # Reconstruct the hierarchy by updating parent-child relationships
    if len(asset_with_children) > 1:
        for child_asset in asset_with_children:
            asset_id = child_asset.get("assetID")
            parent_asset_id = child_asset.get("parentAssetID")

            # Update each asset's parent id
            limble_asset_interface.update_asset(asset_id, parent_asset_id)

    return True


def get_all_child_assets(asset_id):
    """
    Recursively retrieves all child assets for a given asset.

    Args:
        - asset_id (str): The ID of the asset for which to retrieve all child assets.

    Returns:
        - list: A list containing all child assets of the given asset, including the
          asset itself.
    """
    all_assets = []

    def _get_children(asset_id):
        # Get the child assets for the given asset
        child_assets = limble_asset_interface.get_asset_children(asset_id)
        for asset in child_assets:
            all_assets.append(asset)
            # Recursively get children of the current asset
            _get_children(asset["assetID"])

    # Retrieve the parent asset
    parent_asset = limble_asset_interface.get_assets(asset_id)[0]
    all_assets.append(parent_asset)

    # Recursively get child assets
    _get_children(asset_id)
    return all_assets


def update_asset_fields(asset_id, field_values):
    """
    Update asset fields with new values for part number, version, serial number,
    topological name, and status in Limble.

    Args:
        - asset_id (str): The ID of the asset for which to update fields.
        - field_values (dict): A dictionary containing field names as keys and their
          new values as values. Possible keys are "Part Number", "Version",
          "Serial Number", "Topological Name", and "Status".
    """

    # Get the current field values and their IDs
    fields_to_update = {
        field_name: field_value
        for field_name, field_value in field_values.items()
        if field_value is not None
    }
    current_fields = get_field_values(asset_id, set(fields_to_update.keys()))

    for field_name, new_value in fields_to_update.items():
        if field_name in current_fields:
            field_id = current_fields[field_name]["valueID"]

            limble_asset_field_interface.update_asset_field_value(field_id, new_value)


def get_field_values(asset_id, field_names):
    """
    Get the values of specific fields from a list of fields.

    Args:
        - asset_id (int): The ID of the asset.
        - field_names (set): A set of field names for which to retrieve the value IDs.

    Returns:
        - list: A list of dictionaries with FieldName and FieldValue for each matching
          field.
    """
    # Fetch all fields for the given asset
    fields = limble_asset_field_interface.get_asset_fields(asset_id)
    result = {}

    # Loop through each field name in the provided field_names set
    for field_name in field_names:
        for field in fields:
            if field["field"] == field_name:
                result[field_name] = {
                    "valueID": field["valueID"],
                    "value": field["value"],
                }
                break  # Exit the inner loop once a match is found

    return result


def create_placeholder_asset(asset_id, location_id):
    """
    Create a placeholder asset based on the provided asset ID and location ID.

    Retrieves the 'Part Number' field value for the asset, gets the physical item name
    using the part number, creates a new asset with the item name at the specified
    location, links the 'Topological Name' field to the new asset, and returns the ID of
    the new asset.

    Args:
        - asset_id (int): The ID of the asset to create a placeholder for.
        - location_id (str): The ID of the location for the new asset.

    Returns:
        - str: The ID of the newly created placeholder asset.
    """
    asset_child_values = get_field_values(asset_id, ["Alim Part Number"])
    if "Alim Part Number" in asset_child_values:
        item_number = asset_child_values.get("Alim Part Number").get("value")
        item_name = get_physical_item_name(item_number)
        place_holder = limble_asset_interface.create_asset(item_name, location_id)

        spare_id = place_holder["assetID"]

        # Link Topological Name for the asset
        suggested_fields = limble_asset_field_interface.get_suggested_field(
            location_id, "Topological Name"
        )
        suggested_field_id = suggested_fields[0].get("fieldID")
        limble_asset_field_interface.link_asset_field(
            spare_id, location_id, suggested_field_id
        )

        # Link Status for the asset
        suggested_fields = limble_asset_field_interface.get_suggested_field(
            location_id, "Status"
        )
        suggested_field_id = suggested_fields[0].get("fieldID")
        limble_asset_field_interface.link_asset_field(
            spare_id, location_id, suggested_field_id
        )
        update_asset_fields(
            spare_id,
            {
                "Status": "Empty Slot",
            },
        )

    return spare_id


def update_topological_names(asset_id, spare_id):
    """
    Update the topological names of child assets between the given asset and spare
    asset.

    Args:
        - asset_id (str): The ID of the asset whose child assets' topological names will
          be updated.
        - spare_id (str): The ID of the spare asset used to update the topological
          names.
    """
    # Retrieve all child assets for both asset and spare
    asset_children = get_all_child_assets(asset_id)
    spare_children = get_all_child_assets(spare_id)

    # Create a dictionary to map spare children by name
    spare_children_dict = {child["name"]: child for child in spare_children}

    # Iterate through each child asset of the main asset
    for asset_child in asset_children:
        asset_child_name = asset_child["name"]
        asset_child_id = asset_child["assetID"]

        # Check if there is a matching spare child
        if asset_child_name in spare_children_dict:
            spare_child = spare_children_dict[asset_child_name]
            spare_child_id = spare_child["assetID"]

            # Retrieve Topological Names for both asset and spare children
            asset_child_topological_name = (
                get_field_values(asset_child_id, ["Topological Name"]).get(
                    "Topological Name"
                )
                or {}
            ).get("value") or ""

            # Get spare child Topological Names
            spare_child_topological_name = (
                get_field_values(spare_child_id, ["Topological Name"]).get(
                    "Topological Name"
                )
                or {}
            ).get("value") or ""

            # Link Topological Name to spare if not linked
            if not spare_child_topological_name:
                location_id = spare_child.get("locationID")
                suggested_fields = limble_asset_field_interface.get_suggested_field(
                    location_id, "Topological Name"
                )
                suggested_field_id = suggested_fields[0].get("fieldID")
                limble_asset_field_interface.link_asset_field(
                    spare_id, location_id, suggested_field_id
                )

            # Swap and update Topological Names
            update_asset_fields(
                asset_child_id, {"Topological Name": spare_child_topological_name}
            )
            update_asset_fields(
                spare_child_id, {"Topological Name": asset_child_topological_name}
            )
        else:
            # Remove Topological Name if there is no matching spare child
            update_asset_fields(asset_child_id, {"Topological Name": ""})
