"""
LIMBLE API interface for purchase order functions
"""

from ska_ems_api.utils.api_requests import LIMBLE_BASEURL, limble_get_request
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


@handle_request_exceptions
def get_purchase_orders():
    """
    Retrieve purchase orders from Limble.

    Returns:
        dict: A JSON object containing the purchase orders if successful, None
        otherwise.
    """
    api_url = f"{LIMBLE_BASEURL}/po"
    params = {"limit": 9999}

    request = limble_get_request(api_url, params)
    response = request.make_request()

    messages = {"success": "Successfully retrieved purchase orders"}
    return handle_response(response, messages, return_json=True)
