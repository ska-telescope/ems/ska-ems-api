"""
Engineering Data Archive (EDA) interface functions
"""

from ska_ems_api.utils.database import create_low_eda_connection


def get_att_conf(att_names):
    """
    Get the attribute configuration by attribute name.

    Args:
        - att_names (tuple): A tuple of attribute names.

    Returns:
        - list of tuples: A list of tuples where each tuple contains (att_conf_id,
          table_name) for each attribute name found.
    """
    results = []

    att_name_list = ", ".join(f"'{att_name}'" for att_name in att_names)

    query = f"""
        SELECT att_conf_id, att_name, table_name
        FROM att_conf
        WHERE att_name IN ({att_name_list})
    """

    with create_low_eda_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute(query)
            results = cursor.fetchall()

    return results


def get_att_current_values(att_names):
    """
    Get the current values of sensors by attribute names.

    Args:
        - att_names: List of attribute names.

    Returns:
        - List of tuples (att_name, value, time).
    """
    results = []

    config_map = get_att_conf(att_names)

    with create_low_eda_connection() as connection:
        with connection.cursor() as cursor:
            queries = []

            for att_conf_id, att_name, table_name in config_map:
                queries.append(
                    f"""
                        SELECT * FROM (
                            SELECT
                                '{att_name}' AS att_name,
                                value_r AS value,
                                data_time AS time
                            FROM {table_name}
                            WHERE att_conf_id = {att_conf_id}
                            ORDER BY data_time DESC
                            LIMIT 1
                        ) subquery
                    """
                )

            if queries:
                final_query = " UNION ALL ".join(queries)
                cursor.execute(final_query)
                rows = cursor.fetchall()

                results.extend([(row[0], row[1], row[2]) for row in rows])

    return results


def get_att_value_ranges(att_names, start_date, end_date):
    """
    Get the value ranges of components for a list of attributes within a date range.

    Args:
        - att_names: List of attribute names.
        - start_date: The start date for the range.
        - end_date: The end date for the range.

    Returns:
        - Dictionary where each key is the attribute name, and the value is a list of
          tuples (value, time).
    """
    results = []

    with create_low_eda_connection() as connection:
        with connection.cursor() as cursor:
            for att_name in att_names:
                config = get_att_conf(att_name)
                if not config:
                    results[att_name] = None
                    continue

                att_conf_id, table_name = config

                query = f"""
                    SELECT
                        value_r AS value,
                        data_time AS time
                    FROM
                        {table_name}
                    WHERE
                        att_conf_id = {att_conf_id}
                        AND data_time >= '{start_date}'
                        AND data_time <= '{end_date}'
                    ORDER BY
                        data_time DESC
                """

                cursor.execute(query)
                rows = cursor.fetchall()

                for row in rows:
                    value, time = row
                    results.append((att_name, value, time))

    return results
