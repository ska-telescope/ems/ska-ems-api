"""
JIRA API interface functions
"""

import json
import logging
import os

import requests

from ska_ems_api.utils.api_requests import get_request
from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

JIRA_TOKEN = os.getenv("JIRA_TOKEN")
JIRA_API_URL = os.getenv("JIRA_API_URL")

headers = {"Content-Type": "application/json", "Authorization": f"Bearer {JIRA_TOKEN}"}


@handle_request_exceptions
def get_prts_tickets():
    """
    Retrieve all tickets for the SPRTS project from JIRA.

    Returns:
        - dict: A JSON object containing the details of all tickets for the
          SPRTS project.
    """
    api_url = f"{JIRA_API_URL}/search"
    params = {
        "jql": "project = SPRTS",
        "fields": [
            "summary",
            "description",
            "components",
            "status",
            "resolution",
            "created",
            "resolutiondate",
        ],
        "maxResults": 1000,
    }

    request = get_request(api_url, params, headers)
    response = request.make_request()
    messages = {"success": "Successfully retrieved all SPRTS tickets"}
    return handle_response(response, messages, return_json=True)


def get_prts_failure_tickets():
    """
    Retrieve failure tickets from the SPRTS project from JIRA.

    Returns:
        - dict: A JSON object containing information about the failure tickets.
          The object includes the ID, key, summary, description, and components
          of each ticket.
    """
    try:
        api_url = f"{JIRA_API_URL}/search"
        params = {
            "jql": "project = SPRTS AND issuetype = Failure",
            "fields": ["id", "key", "summary", "description", "components"],
        }

        response = requests.get(api_url, headers=headers, params=params, timeout=10)

        if response.status_code == 200:
            return response.json()
        if response.status_code == 401:
            logging.error("401 Unauthorized Error! Check your credentials.")
        else:
            logging.error(
                "Unexpected response. Status code: %s: %s",
                response.status_code,
                response.text,
            )
    except requests.exceptions.RequestException as e:
        logging.error("Request exception occurred: %s", e)

    return None


def update_ticket(key):
    """
    Update a JIRA ticket with the specified key.

    Args:
        - key (str): The key of the JIRA ticket to be updated.

    Returns:
        - dict or None: If the ticket is successfully updated, returns the JSON
          response. If there is a 401 Unauthorized Error, returns None.
          If there is any other error, returns None.
    """
    try:
        payload = json.dumps({"fields": {"status": "Test"}})

        api_url = f"{JIRA_API_URL}/issue/{key}"

        response = requests.put(api_url, headers=headers, data=payload, timeout=10)

        if response.status_code == 204:
            return response.json()
        if response.status_code == 401:
            logging.error("401 Unauthorized Error! Check your credentials.")
        else:
            logging.error(
                """Failed to update work order link.
                Status code: %s, Response: %s""",
                response.status_code,
                response.text,
            )
    except requests.exceptions.RequestException as e:
        logging.error("Request exception occurred: %s", e)

    return None


def get_ticket_work_order(key):
    """
    Retrieve the work orders remote link on a JIRA ticket.

    Args:
        - key (str): The key of the JIRA ticket for which to retrieve the work
          orders.

    Returns:
        - dict: A dictionary containing work order information if the request
          is successful, otherwise None.
    """
    try:
        api_url = f"{JIRA_API_URL}/issue/{key}/remotelink"

        response = requests.get(api_url, headers=headers, timeout=10)

        if response.status_code == 200:
            return response.json()
        if response.status_code == 401:
            logging.error("401 Unauthorized Error! Check your credentials.")
        else:
            logging.error(
                """Failed to update work order link.
                Status code: %s, Response: %s""",
                response.status_code,
                response.text,
            )
    except requests.exceptions.RequestException as e:
        logging.error("Request exception occurred: %s", e)

    return None


def create_ticket_work_order(key, url, title, summary):
    """
    Create a work order remote link in JIRA for a given issue key.

    Args:
        - key (str): The issue key for which the work order link will be
          created. url (str): The work order URL to be linked. title (str): The
          title of the work order. summary (str): The summary of the work
          order.

    Returns:
        - dict: A JSON response containing the details of the created work
          order link.
    """
    try:
        payload = json.dumps(
            {
                "object": {
                    "url": url,
                    "title": title,
                    "summary": summary,
                    "icon": {
                        "url16x16": "https://limblecmms.com/wp-content/uploads/LimbleOnlyLogoSquare-1-150x150.png",  # noqa: E501  # pylint: disable=C0301
                        "title": "Limble",
                    },
                    "status": {
                        "resolved": False,
                        "icon": {
                            "url16x16": "https://jira.skatelescope.org/secure/viewavatar?size=xsmall&avatarId=10308&avatarType=issuetype",  # noqa: E501  # pylint: disable=C0301
                            "title": "To Do",
                            "link": "https://app.limblecmms.com",
                        },
                    },
                }
            }
        )

        api_url = f"{JIRA_API_URL}/issue/{key}/remotelink"

        response = requests.post(api_url, headers=headers, data=payload, timeout=10)

        if response.status_code == 201:
            return response.json()
        if response.status_code == 401:
            logging.error("401 Unauthorized Error! Check your credentials.")
        else:
            logging.error(
                """Failed to create work order link.
                Status code: %s, Response: %s""",
                response.status_code,
                response.text,
            )
    except requests.exceptions.RequestException as e:
        logging.error("Request exception occurred: %s", e)

    return None


def update_ticket_work_order(key, work_order_link):
    """
    Update the work order remote link for a specific JIRA ticket.

    Args:
        work_order_link (dict): A dictionary containing the details of the
        work order link. Required keys:
            - link_id (str): The ID of the remote link to update.
            - url (str): The URL of the work order.
            - title (str): The title of the work order.
            - summary (str): The summary of the work order.
            - resolved (bool): Indicates if the work order is resolved.
    """

    try:
        link_id = work_order_link.id
        url = work_order_link.url
        title = work_order_link.title
        summary = work_order_link.summary
        resolved = work_order_link.resolved

        status_icon = "https://jira.skatelescope.org/secure/viewavatar?size=xsmall&avatarId=10308&avatarType=issuetype"  # noqa: E501  # pylint: disable=C0301
        status_title = "To Do"

        if resolved:
            status_icon = "https://jira.skatelescope.org/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype"  # noqa: E501  # pylint: disable=C0301
            status_title = "Done"

        payload = json.dumps(
            {
                "object": {
                    "url": url,
                    "title": title,
                    "summary": summary,
                    "icon": {
                        "url16x16": "https://limblecmms.com/wp-content/uploads/LimbleOnlyLogoSquare-1-150x150.png",  # noqa: E501  # pylint: disable=C0301
                        "title": "Limble",
                    },
                    "status": {
                        "resolved": resolved,
                        "icon": {
                            "url16x16": status_icon,
                            "title": status_title,
                            "link": "https://app.limblecmms.com",
                        },
                    },
                }
            }
        )

        api_url = f"{JIRA_API_URL}/issue/{key}/remotelink/{link_id}"

        response = requests.put(api_url, headers=headers, data=payload, timeout=10)

        if response.status_code == 204:
            logging.info("Successfully updated work order link for %s", key)
        if response.status_code == 401:
            logging.error("401 Unauthorized Error! Check your credentials.")
        else:
            logging.error(
                """Failed to update work order link.
                Status code: %s, Response: %s""",
                response.status_code,
                response.text,
            )
    except requests.exceptions.RequestException as e:
        logging.error("Request exception occurred: %s", e)


def delete_ticket_work_order(key, link_id):
    """
    Delete a work order remote link from a JIRA ticket.

    Args:
        - key (str): The key of the JIRA ticket from which the work order link
          needs to be removed.
        - id (str): The ID of the work order link to be
          deleted.
    """
    try:
        api_url = f"{JIRA_API_URL}/issue/{key}/remotelink/{link_id}"

        response = requests.delete(api_url, headers=headers, timeout=10)

        if response.status_code == 204:
            logging.info("Successfully removed work order link for %s", key)
        if response.status_code == 401:
            logging.error("401 Unauthorized Error! Check your credentials.")
        else:
            logging.error(
                """Failed to delete work order link.
                Status code: %s, Response: %s""",
                response.status_code,
                response.text,
            )
    except requests.exceptions.RequestException as e:
        logging.error("Request exception occurred: %s", e)
