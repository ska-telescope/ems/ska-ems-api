;WITH InitialCTE AS (
    -- Base case: Include the root item with level 0
    SELECT 
        NULL as parent_id, 
        CASE 
            WHEN p.item_class = 'F' THEN 'Fabricated' 
            WHEN p.item_class = 'P' THEN 'Procured' 
            ELSE p.item_class 
        END as item_class,
        NULL as seq_number,
        NULL as quantity_per,
        p.item_id as component_id, 
        p.item_number, 
        p.ver, 
        p.description as description,
		p.config_item,
        u.code as uom,
		CASE 
            WHEN p.status = 'N' THEN 'Not Approved' 
            WHEN p.status = 'R' THEN 'Approved' 
            ELSE p.status 
        END as status,
		p.software,
		p.serialised,
		p.under_change,
		p.main_equipment,
        0 as level,
        CAST(0 AS VARCHAR(MAX)) AS path
    FROM [dbo].[items] p
    INNER JOIN [dbo].[uom_units] u ON u.unit_id = p.unit_id
    WHERE p.item_number = '000-000000'
),

RecursiveCTE AS (
    -- First level children
    SELECT 
        ps.parent_id, 
        CASE 
            WHEN p.item_class = 'F' THEN 'Fabricated' 
            WHEN p.item_class = 'P' THEN 'Procured' 
            ELSE p.item_class 
        END as item_class,
        ps.seq_number,
        CAST(ps.quantity_per AS INT) as quantity_per,
        ps.component_id, 
        c.item_number, 
        c.ver, 
        c.description as description,
		c.config_item,
        u.code as uom,
		CASE 
            WHEN p.status = 'N' THEN 'Not Approved' 
            WHEN p.status = 'R' THEN 'Approved' 
            ELSE p.status 
        END as status,
		c.software,
		c.serialised,
		c.under_change,
		c.main_equipment,
        1 as level,
        CAST(CAST(1 AS VARCHAR(MAX)) + '>' + CAST(ps.seq_number AS VARCHAR(MAX)) AS VARCHAR(MAX)) AS path
    FROM [dbo].[product_structures] ps
    INNER JOIN [dbo].[items] p ON p.item_id = ps.parent_id
    INNER JOIN [dbo].[items] c ON c.item_id = ps.component_id
    INNER JOIN [dbo].[uom_units] u ON u.unit_id = c.unit_id
    INNER JOIN [dbo].[documents] d ON d.document_id = ps.control_id
    WHERE ps.parent_id IN (SELECT component_id FROM InitialCTE)
      AND d.middle = 'PLS' AND d.current_revision = 'Y'
                
    UNION ALL
                
    -- Recursive case: Handle subsequent levels
    SELECT 
        ps.parent_id, 
        CASE 
            WHEN p.item_class = 'F' THEN 'Fabricated' 
            WHEN p.item_class = 'P' THEN 'Procured' 
            ELSE p.item_class 
        END as item_class,
        ps.seq_number,
        CAST(ps.quantity_per AS INT) as quantity_per,
        ps.component_id, 
        c.item_number, 
        c.ver, 
        c.description,
		c.config_item,
        u.code,
		CASE 
            WHEN p.status = 'N' THEN 'Not Approved' 
            WHEN p.status = 'R' THEN 'Approved' 
            ELSE p.status 
        END as status,
		c.software,
		c.serialised,
		c.under_change,
		c.main_equipment,
        r.level + 1,
        CAST(r.path + '>' + CAST(ps.seq_number AS VARCHAR(MAX)) AS VARCHAR(MAX)) AS path
    FROM [dbo].[product_structures] ps
    INNER JOIN [dbo].[items] c ON c.item_id = ps.component_id
    INNER JOIN RecursiveCTE r ON ps.parent_id = r.component_id
    INNER JOIN [dbo].[items] p ON p.item_id = ps.parent_id
    INNER JOIN [dbo].[uom_units] u ON u.unit_id = c.unit_id
    INNER JOIN [dbo].[documents] d ON d.document_id = ps.control_id
    WHERE d.middle = 'PLS' AND d.current_revision = 'Y'
)

SELECT
    id,
    parent_id,
    level,
    item_number,
    ver,
    description,
    config_item,
    seq_number,
    uom,
    status,
    quantity_per,
    item_class,
    software,
    serialised,
    under_change,
    main_equipment
FROM (
    SELECT
        component_id as id,
        parent_id,
        level,
        item_number,
        ver,
        description,
        config_item,
        seq_number,
        uom,
        status,
        quantity_per,
        item_class,
        software,
        serialised,
        under_change,
        main_equipment,
        path
    FROM InitialCTE
    UNION ALL
    SELECT 
        component_id,
        parent_id,
        level,
        item_number,
        ver,
        description,
        config_item,
        seq_number,
        uom,
        status,
        quantity_per,
        item_class,
        software,
        serialised,
        under_change,
        main_equipment,
        path
    FROM RecursiveCTE
) AS CombinedCTE
ORDER BY path;
