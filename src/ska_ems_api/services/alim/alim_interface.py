"""
ALIM API interface functions
"""

import logging
import os

import pymssql

from ska_ems_api.utils.database import create_alim_connection
from ska_ems_api.utils.helpers import read_query_from_file


def get_product_structure():
    """
    Retrieve product structure data from ALIM.

    Returns:
        list of tuples or None: A list of tuples containing the product
        structure data if successful, None otherwise.
    """
    connection = create_alim_connection()
    if not connection:
        return None

    with connection.cursor() as cursor:
        sql_file_path = os.path.join(os.path.dirname(__file__), "alim_pbs.sql")
        sql_alim_pbs_query = read_query_from_file(sql_file_path)

        try:
            cursor.execute(sql_alim_pbs_query)
            rows = cursor.fetchall()
        except pymssql.Error as e:
            logging.error(
                "A database error occurred during query ALIM PBS execution: %s", e
            )
            rows = None

        return rows
