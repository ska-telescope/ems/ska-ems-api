"""
Authentication services
"""

from datetime import datetime

import psycopg2
import psycopg2.extras
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from ska_ems_api.utils.database import create_ems_connection
from ska_ems_api.utils.token_utils import to_epoch

router = APIRouter()

security = HTTPBearer()


def validate_token(
    credentials: HTTPAuthorizationCredentials = Depends(security),
) -> dict:
    """
    Check if the token is in the database and if it's still valid (not expired).
    The expiration date is stored as epoch time.

    Returns: The token record if it exists and is valid (not expired), raises
    HTTPException otherwise
    """
    conn = create_ems_connection()

    sql_query = """SELECT id, client_id, token, expires_at, scope, description,
    creation_date, last_used FROM api_gateway_tokens u WHERE u.token = %s"""

    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(
        sql_query,
        [
            credentials.credentials,
        ],
    )
    token_record = cursor.fetchone()
    if token_record is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token"
        )

    current_time = to_epoch(datetime.now())
    if token_record["expires_at"] is not None:

        # Check if the token has expired
        if current_time > token_record["expires_at"]:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail="Token has expired"
            )

    update_last_used_sql = """
        UPDATE api_gateway_tokens
        SET last_used = (%s)
        WHERE id = (%s)
    """

    cursor.execute(
        update_last_used_sql,
        [
            current_time,
            token_record["id"],
        ],
    )
    conn.commit()
    conn.close()
    return token_record


def require_read_permission(
    credentials: HTTPAuthorizationCredentials = Depends(security),
):
    """
    Validate if the token has the required READ permission.
    """
    token_record = validate_token(credentials)

    if token_record["scope"] != "READ_ONLY" and token_record["scope"] != "READ_WRITE":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Insufficient permissions"
        )

    return token_record


def require_write_permission(
    credentials: HTTPAuthorizationCredentials = Depends(security),
):
    """
    Validate if the token has the required WRITE permission.
    """

    token_record = validate_token(credentials)

    if token_record["scope"] != "READ_WRITE":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Insufficient permissions"
        )

    return token_record
