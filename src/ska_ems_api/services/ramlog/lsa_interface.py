"""
RamLog interface functions
"""

import logging
import os

# import pyodbc
import pymssql

from ska_ems_api.utils.database import create_ramlog_connection
from ska_ems_api.utils.helpers import read_query_from_file

# def get_product_breakdown_structure():
#     """
#     Retrieve product breakdown structure data from RamLog.

#     Returns:
#         list of tuples or None: A list of tuples containing the product
#         structure data if successful, None otherwise.
#     """

# # Connection details
# server = "127.0.0.1\\SQLEXPRESS"
# user = "sa"
# password = "SQLadmin"
# database = "SKA"

# try:
#     print("Connect")
#     # Establish connection
#     conn = pymssql.connect(
#         server=server, user=user, password=password, database=database
#     )
#     print("Connected")
#     cursor = conn.cursor()

#     # Execute a query
#     cursor.execute("SELECT TOP 10 * FROM your_table_name")

#     # Fetch results
#     for row in cursor.fetchall():
#         print(row)

#     # Close the connection
#     conn.close()
# except pymssql.Error as e:
#     print(f"Error connecting to SQL Server: {e}")


def get_product_breakdown_structure():
    """
    Retrieve product breakdown structure data from RamLog.

    Returns:
        list of tuples or None: A list of tuples containing the product
        structure data if successful, None otherwise.
    """
    connection = create_ramlog_connection()
    if not connection:
        return None

    with connection.cursor() as cursor:
        sql_file_path = os.path.join(os.path.dirname(__file__), "lsa_pbs.sql")
        sql_lsa_pbs_query = read_query_from_file(sql_file_path)

        try:
            cursor.execute(sql_lsa_pbs_query)
            rows = cursor.fetchall()
        except pymssql.Error as e:
            logging.error(
                "A database error occurred during query LSA PBS execution: %s", e
            )
            rows = None

        return rows
