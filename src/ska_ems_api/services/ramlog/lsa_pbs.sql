SELECT Systems.EIACode, FBSPBSparentLink.ExternalLCN, FBSPBS.LCNName, PBSItem.QtyPerAssembly, PBSItem.LRU, RefNumber, CageCode, CageName
FROM Systems 
INNER JOIN FBSPBSparentLink ON FBSPBSparentLink.SystemID = Systems.SystemID
INNER JOIN FBSPBS ON FBSPBS.LCNID = FBSPBSparentLink.LCNID
LEFT OUTER JOIN PBSItem ON PBSItem.LCNID = FBSPBS.LCNID 
LEFT OUTER JOIN ItemsLIB ON ItemsLIB.ItemID = PBSItem.ItemID
LEFT OUTER JOIN SuppliersLIB ON SuppliersLIB.CageID = ItemsLIB.CageID
WHERE LCNtype = 'P'