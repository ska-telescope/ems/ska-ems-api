"""
Database queries.
"""

import logging

import psycopg2

from ska_ems_api.utils.database import create_ems_connection


def create_issue_work_order_link(key, link_id, task_id):
    """
    Create a link between an issue, work order, and task in the EMS database.

    Args:
        - key (str): The key to identify the link.
        - link_id (int): The ID of the link.
        - task_id (int): The ID of the task.

    Returns:
        - int: The number of rows affected by the insert operation.
    """
    conn = create_ems_connection()
    cursor = conn.cursor()

    insert_query = """
        INSERT INTO prts_issue_work_orders (jira_key, jira_link_id, limble_task_id)
        VALUES (%s, %s, %s)
    """
    rows = cursor.execute(insert_query, (key, link_id, task_id))

    conn.commit()
    cursor.close()
    conn.close()
    return rows


def delete_issue_work_order_link(key, link_id):
    """
    Delete a specific link between an issue and a work order in the EMS
    database.

    Args:
        - key (str): The Jira key of the issue.
        - link_id (int): The Jira link ID of the work order.
    """
    conn = create_ems_connection()

    try:
        with conn.cursor() as cursor:
            delete_query = """
                DELETE FROM prts_issue_work_orders
                WHERE jira_key = %s AND jira_link_id = %s
            """
            cursor.execute(delete_query, (key, link_id))
            conn.commit()

    except (
        psycopg2.DatabaseError,
        psycopg2.OperationalError,
    ) as e:
        logging.error("Error deleting issue work order link: %s", e)
        conn.rollback()
    finally:
        conn.close()


def get_physical_item_name(part_number):
    """
    Retrieve the physical item name from the EMS database based on the provided part
    number.

    Args:
        - part_number (str): The part number of the physical item.

    Returns:
        str or None: The description of the physical item if found, None otherwise.
    """
    conn = create_ems_connection()

    select_query = (
        """SELECT description FROM configuration_control_items WHERE item_number = %s"""
    )
    cursor = conn.cursor()
    cursor.execute(select_query, (part_number,))

    result = cursor.fetchone()
    conn.close()

    if result:
        return result[0]

    return None
