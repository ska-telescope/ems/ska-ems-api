"""
Token Authentication helper functions
"""


def to_epoch(dt):
    """
    Convert a timestamp to Epoch.

    Args:
        - dt (datetime): The datetime object to convert.

    Returns:
        - int: The corresponding Epoch time as an integer, or None if dt is None.
    """
    return int(dt.timestamp()) if dt else None
