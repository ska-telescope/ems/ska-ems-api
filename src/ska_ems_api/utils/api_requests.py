"""
This module provides a utility function to make HTTP requests.
"""

import os
from dataclasses import dataclass
from typing import Any, Optional

import requests
from requests.auth import HTTPBasicAuth

LIMBLE_USERNAME = os.getenv("LIMBLE_CLIENT_ID")
LIMBLE_PASSWORD = os.getenv("LIMBLE_CLIENT_SECRET")
LIMBLE_BASEURL = os.getenv("LIMBLE_API_URL")


@dataclass
class ApiRequest:
    """
    Dataclass object for an API request.
    """

    # pylint: disable=too-many-instance-attributes
    api_url: str
    headers: Optional[dict[str, str]] = None
    username: Optional[str] = None
    password: Optional[str] = None
    payload: Optional[str] = None
    params: Optional[dict[str, int | Any]] = None
    method: str = "POST"
    timeout: int = 10

    def make_request(self):
        """
        Make an HTTP request.

        Returns:
        - Response: The HTTP response object.

        Raises:
        - ValueError: If an unsupported HTTP method is provided.
        """
        method = self.method.upper()

        auth = None
        if self.username and self.password:
            auth = HTTPBasicAuth(self.username, self.password)

        if method == "GET":
            return requests.get(
                self.api_url,
                headers=self.headers,
                auth=auth,
                params=self.params,
                timeout=self.timeout,
            )
        if method == "POST":
            return requests.post(
                self.api_url,
                headers=self.headers,
                auth=auth,
                data=self.payload,
                timeout=self.timeout,
            )
        if method == "PUT":
            return requests.put(
                self.api_url,
                headers=self.headers,
                auth=auth,
                data=self.payload,
                timeout=self.timeout,
            )
        if method == "DELETE":
            return requests.delete(
                self.api_url,
                headers=self.headers,
                auth=auth,
                timeout=self.timeout,
            )
        if method == "PATCH":
            return requests.patch(
                self.api_url,
                headers=self.headers,
                auth=auth,
                data=self.payload,
                timeout=self.timeout,
            )

        raise ValueError(f"Unsupported HTTP method: {method}")


def get_request(api_url, params, headers=None):
    """
    Generic Get request
    """
    return ApiRequest(
        api_url=api_url,
        headers=headers,
        params=params,
        method="GET",
    )


def limble_get_request(api_url, params):
    """
    Generic Limble Get request
    """
    return ApiRequest(
        api_url=api_url,
        username=LIMBLE_USERNAME,
        password=LIMBLE_PASSWORD,
        params=params,
        method="GET",
    )


def limble_post_request(api_url, payload):
    """
    Generic Limble Post request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        username=LIMBLE_USERNAME,
        password=LIMBLE_PASSWORD,
        payload=payload,
        method="POST",
    )


def limble_patch_request(api_url, payload):
    """
    Generic Limble Patch request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        username=LIMBLE_USERNAME,
        password=LIMBLE_PASSWORD,
        payload=payload,
        method="PATCH",
    )


def limble_put_request(api_url, payload):
    """
    Generic Limble Put request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        username=LIMBLE_USERNAME,
        password=LIMBLE_PASSWORD,
        payload=payload,
        method="PUT",
    )


def limble_delete_request(api_url):
    """
    Generic Limble Put request
    """
    headers = {"Content-Type": "application/json"}

    return ApiRequest(
        api_url=api_url,
        headers=headers,
        username=LIMBLE_USERNAME,
        password=LIMBLE_PASSWORD,
        method="DELETE",
    )
