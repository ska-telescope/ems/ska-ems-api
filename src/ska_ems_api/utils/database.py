"""
Initialize database connections
"""

import logging
import os

import psycopg2
import pymssql

POSTGRESS_HOST = os.getenv("POSTGRESS_HOST")
POSTGRESS_PORT = os.getenv("POSTGRESS_PORT")
POSTGRESS_DATABASE = os.getenv("POSTGRESS_DATABASE")
POSTGRESS_USERNAME = os.getenv("POSTGRESS_USERNAME")
POSTGRESS_PASSWORD = os.getenv("POSTGRESS_PASSWORD")


def create_ems_connection():
    """
    Create a connection to the EMS database.

    Returns:
        psycopg2 connection object: A connection to the PostgreSQL database if
        successful, None otherwise.
    """
    try:
        connection = psycopg2.connect(
            host=POSTGRESS_HOST,
            port=POSTGRESS_PORT,
            dbname=POSTGRESS_DATABASE,
            user=POSTGRESS_USERNAME,
            password=POSTGRESS_PASSWORD,
        )
        return connection
    except psycopg2.Error as e:
        logging.error({"An error creating a connection to postgress": str(e)})
        return None


ALIM_SERVER = os.getenv("ALIM_SERVER")
ALIM_DATABASE = os.getenv("ALIM_DATABASE")
ALIM_USERNAME = os.getenv("ALIM_USERNAME")
ALIM_PASSWORD = os.getenv("ALIM_PASSWORD")


def create_alim_connection():
    """
    Create a connection to an ALIM replica database.

    Returns:
        pymssql.Connection: A connection object to the ALIM replica database if
        successful, None otherwise.
    """

    if not all([ALIM_SERVER, ALIM_DATABASE, ALIM_USERNAME, ALIM_PASSWORD]):
        logging.error("Missing required environment variables for ALIM connection")
        return None

    try:
        # pylint: disable=no-member
        connection = pymssql.connect(
            server=ALIM_SERVER,
            user=ALIM_USERNAME,
            password=ALIM_PASSWORD,
            database=ALIM_DATABASE,
        )

        logging.info("Connection to ALIM replica database successful")
        return connection
    except pymssql.Error as e:
        logging.error("An error creating a connection to sql server %s", e)
        return None


RAMLOG_SERVER = os.getenv("RAMLOG_SERVER")
RAMLOG_DATABASE = os.getenv("RAMLOG_DATABASE")
RAMLOG_USERNAME = os.getenv("RAMLOG_USERNAME")
RAMLOG_PASSWORD = os.getenv("RAMLOG_PASSWORD")


def create_ramlog_connection():
    """
    Create a connection to a RamLog Information Edition database.

    Returns:
        pymssql.Connection: A connection object to the RamLog Information Edition
        database if successful, None otherwise.
    """

    if not all([RAMLOG_SERVER, RAMLOG_DATABASE, RAMLOG_USERNAME, RAMLOG_PASSWORD]):
        logging.error("Missing required environment variables for RamLog connection")
        return None

    try:
        # pylint: disable=no-member
        connection = pymssql.connect(
            server=RAMLOG_SERVER,
            user=RAMLOG_USERNAME,
            password=RAMLOG_PASSWORD,
            database=RAMLOG_DATABASE,
        )

        logging.info("Connection to RamLog database successful")
        return connection
    except pymssql.Error as e:
        logging.error("An error creating a connection to sql server %s", e)
        return None


LOW_EDA_HOST = os.getenv("LOW_EDA_HOST")
LOW_EDA_PORT = os.getenv("LOW_EDA_PORT")
LOW_EDA_DATABASE = os.getenv("LOW_EDA_DATABASE")
LOW_EDA_USERNAME = os.getenv("LOW_EDA_USERNAME")
LOW_EDA_PASSWORD = os.getenv("LOW_EDA_PASSWORD")


def create_low_eda_connection():
    """
    Create a connection to the Low EDA.

    Returns:
        pymssql.Connection: A connection object to the Low EDA if successful, None
        otherwise.
    """
    connection = None
    try:
        connection = psycopg2.connect(
            host=LOW_EDA_HOST,
            port=LOW_EDA_PORT,
            database=LOW_EDA_DATABASE,
            user=LOW_EDA_USERNAME,
            password=LOW_EDA_PASSWORD,
        )
        return connection
    except psycopg2.Error as e:
        logging.error({"An error creating a connection to postgress": str(e)})
        return None
