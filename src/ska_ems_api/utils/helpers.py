"""Module containing helper funcions"""

import logging
import os
import re
from datetime import datetime

from bs4 import BeautifulSoup


def read_query_from_file(file_path):
    """
    Reads and returns the content of a SQL query file.

    Args:
        - file_path (str): The path to the SQL query file.

    Returns:
        str: The content of the SQL query file as a string.
    """
    if not os.path.exists(file_path):
        logging.error("SQL file not found")
        return None

    with open(file_path, "r", encoding="utf-8") as file:
        query = file.read()
    return query


def convert_to_epoch(date_str: str) -> int:
    """
    Convert a date string to epoch time. Returns None if date_str is None or invalid.
    """
    if date_str:
        try:
            date_obj = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%f%z")
            return int(date_obj.timestamp())
        except ValueError as e:
            print(e)
            return None
    return None


def parse_html(html):
    """
    Extracting data_module_code, part_number, support_equipment and spares from html
    """
    soup = BeautifulSoup(html, "html.parser")
    tables = soup.find_all("table")

    issue_number = None
    part_number = None
    support_equipment = []
    spares = []

    if tables and len(tables) > 1:
        applicability_text = tables[1].find(text=re.compile("Applicability:"))

        if applicability_text:
            td_elements = tables[1].find_all("td")
            if td_elements:
                issue_number = (
                    td_elements[0].get_text(strip=True).split(":")[-1].strip()
                )

            # Extract part number based on the applicability text
            applicability_table_cell = applicability_text.find_parent("td")
            if applicability_table_cell:
                applicability_full_text = applicability_table_cell.get_text(strip=True)
                applicability_parts = applicability_full_text.split(":")
                if len(applicability_parts) > 1:
                    part_number_candidate = applicability_parts[1].strip()
                    if part_number_candidate:
                        part_number = part_number_candidate

    return [issue_number, part_number, support_equipment, spares]
