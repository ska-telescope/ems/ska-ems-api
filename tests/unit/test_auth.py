"""
Test the Authorization service
"""

from datetime import datetime
from unittest.mock import MagicMock, patch

import pytest
from fastapi import HTTPException, status
from fastapi.testclient import TestClient

from ska_ems_api.main import app
from ska_ems_api.services.auth import (
    require_read_permission,
    require_write_permission,
    validate_token,
)

client = TestClient(app)


def mock_token_record(expired=False, scope="READ_ONLY"):
    """
    Mock a token record
    """
    current_time = int(datetime.now().timestamp())
    token_record = {
        "id": 1,
        "client_id": 123,
        "token": "valid_token",
        "expires_at": current_time - 10000 if expired else current_time + 10000,
        "scope": scope,
        "description": "Test token",
        "creation_date": current_time - 1000,
        "last_used": current_time - 500,
    }
    return token_record


@patch("ska_ems_api.services.auth.validate_token")
def test_require_read_permission_valid(mock_validate_token):
    """
    Test that the required read permissions are valid
    """
    mock_validate_token.return_value = mock_token_record(False, "READ_ONLY")

    credentials = MagicMock()
    token_record = require_read_permission(credentials)

    assert token_record["scope"] == "READ_ONLY"


@patch("ska_ems_api.services.auth.validate_token")
def test_require_read_permission_read_write(mock_validate_token):
    """
    Test that a valid token with READ_WRITE scope also allow read access
    """
    mock_validate_token.return_value = mock_token_record(False, "READ_WRITE")

    credentials = MagicMock()
    token_record = require_read_permission(credentials)

    assert token_record["scope"] == "READ_WRITE"


@patch("ska_ems_api.services.auth.validate_token")
def test_require_write_permission_valid(mock_validate_token):
    """
    Test a valid token with READ_WRITE scope
    """
    mock_validate_token.return_value = mock_token_record(False, "READ_WRITE")

    credentials = MagicMock()
    token_record = require_write_permission(credentials)

    assert token_record["scope"] == "READ_WRITE"


@patch("ska_ems_api.services.auth.validate_token")
def test_require_write_permission_insufficient(mock_validate_token):
    """
    Test a token with READ_ONLY permission is insufficient for write access
    """
    mock_validate_token.return_value = mock_token_record(False, "READ_ONLY")

    credentials = MagicMock()

    with pytest.raises(HTTPException) as exc_info:
        require_write_permission(credentials)

    assert exc_info.value.status_code == status.HTTP_401_UNAUTHORIZED
    assert exc_info.value.detail == "Insufficient permissions"


@patch("ska_ems_api.services.auth.create_ems_connection")
def test_token_expired(mock_create_conn):
    """
    Test that an expired token is not permitted
    """
    conn = MagicMock()
    cursor = MagicMock()
    conn.cursor.return_value = cursor

    cursor.execute.return_value = MagicMock()
    cursor.fetchone.return_value = mock_token_record(True, "READ_WRITE")
    mock_create_conn.return_value = conn

    credentials = MagicMock()
    credentials.credentials = "expired_token"

    with pytest.raises(HTTPException) as exc_info:
        validate_token(credentials)

    assert exc_info.value.status_code == status.HTTP_401_UNAUTHORIZED
    assert exc_info.value.detail == "Token has expired"
