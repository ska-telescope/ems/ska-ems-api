"""
Configuration control unit tests
"""

from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_ems_api.main import app

client = TestClient(app)

mock_data = [
    (
        138,
        None,
        0,
        "000-000000",
        "01.00",
        "SKA Observatory",
        "Y",
        None,
        "EACH",
        "Not Approved",
        "Fabricated",
        "Y",
        "N",
        "N",
        "Y",
        "N",
    ),
    (
        3895,
        138,
        1,
        "000-010000",
        "01.00",
        "SKA Observatory User System",
        "Y",
        1,
        "EACH",
        "Not Approved",
        "Fabricated",
        "Y",
        "N",
        "N",
        "N",
        "N",
    ),
    (
        3460,
        3895,
        2,
        "000-000001",
        "01.00",
        "Common Software Services",
        "Y",
        1,
        "EACH",
        "Not Approved",
        "Fabricated",
        "Y",
        "N",
        "N",
        "N",
        "N",
    ),
]


def test_successful_retrieval_of_product_structure():
    """
    Test the successful retrieval of product structure from the API.

    This function mocks the data returned by the ALIM interface and sends a GET
    request to the '/pbs' endpoint. It then asserts the response status code,
    content type, and content disposition. It also checks if the CSV response
    contains the expected number of lines (header + data rows) and validates
    the CSV header.
    """
    with patch(
        "ska_ems_api.services.alim.alim_interface.get_product_structure",
        return_value=mock_data,
    ):
        response = client.get("/pbs")
        assert response.status_code == 200
        assert response.headers["content-type"] == "text/csv; charset=utf-8"
        assert (
            response.headers["content-disposition"]
            == "attachment; filename=product_structure.csv"
        )

        csv_lines = response.text.splitlines()
        assert len(csv_lines) == len(mock_data) + 1  # header + data rows

        # Check csv header
        expected_header = (
            "id,parent_id,level,item_number,version,"
            "description,config_item,seq_number,uom,status,quantity_per,"
            "item_class,software,serialised,under_change,main_equipment"
        )
        assert csv_lines[0] == expected_header


def test_database_connection_failure():
    """
    Test for checking the behavior of the API when there is a database
    connection failure. It sends a GET request to the '/pbs' endpoint and
    expects a status code of 500. It also checks if the response JSON contains
    the expected error detail message.
    """
    response = client.get("/pbs")
    assert response.status_code == 500
    assert response.json() == {"detail": "Failed to connect to the database"}
