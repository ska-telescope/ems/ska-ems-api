"""
Test the Maintenance Management endpoints
"""

from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_ems_api.main import app
from ska_ems_api.services.auth import require_read_permission, require_write_permission
from ska_ems_api.services.cmms import (
    limble_asset,
    limble_asset_field_interface,
    limble_asset_interface,
    limble_location_interface,
    limble_task_interface,
)

client = TestClient(app)


def mock_required_permissions():
    """
    Mock function to replace the permissions dependency
    """
    return None


def get_maintenance_schedule():
    """
    Test retrieving maintenance schedule.
    """
    with patch.object(limble_task_interface, "get_tasks") as mock_get_tasks:
        mock_get_tasks.return_value = [
            {
                "taskID": 1,
                "name": "Test Task",
                "description": "Test Description",
                "locationID": 10,
                "assetID": 100,
                "status": 0,
                "priority": 1,
                "startDate": 1723521600,
                "due": 1723708800,
                "createdDate": 1723000747,
                "dateCompleted": 0,
            }
        ]

        response = client.get("/maintenance-schedule?include_mid=true&include_low=true")
        assert response.status_code == 200
        assert response.json() == [
            {
                "task_id": 1,
                "name": "Test Task",
                "description": "Test Description",
                "location_id": 10,
                "asset_id": 100,
                "status": 0,
                "priority": 1,
                "start_date": 1723521600,
                "due_date": 1723708800,
                "created_date": 1723000747,
                "date_completed": 0,
            }
        ]


def test_get_regions():
    """
    Test retrieving regions.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions

    with patch.object(limble_location_interface, "get_regions") as mock_get_regions:
        mock_get_regions.return_value = [
            {
                "regionID": 1,
                "regionName": "South Africa",
                "parentRegionID": 0,
            }
        ]

        response = client.get("/limble/regions")
        assert response.status_code == 200
        assert response.json() == [
            {
                "region_id": 1,
                "name": "South Africa",
                "parent_region_id": 0,
            }
        ]


def test_get_locations():
    """
    Test retrieving locations.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions

    with patch.object(limble_location_interface, "get_locations") as mock_get_locations:
        mock_get_locations.return_value = [
            {
                "locationID": 1,
                "name": "EOC",
                "regionID": 0,
            }
        ]

        response = client.get("/limble/locations")
        assert response.status_code == 200
        assert response.json() == [
            {
                "location_id": 1,
                "name": "EOC",
                "region_id": 0,
            }
        ]


def test_get_assets():
    """
    Test retrieving assets.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions

    with patch.object(limble_asset_interface, "get_assets") as mock_get_assets:
        mock_get_assets.return_value = [
            {
                "assetID": 7145,
                "name": "Coffee Machine #1",
                "parentAssetID": 0,
                "locationID": 33395,
            }
        ]

        response = client.get("/limble/assets")
        assert response.status_code == 200
        assert response.json() == [
            {
                "asset_id": 7145,
                "name": "Coffee Machine #1",
                "parent_asset_id": 0,
                "location_id": 33395,
            }
        ]


def test_get_asset_fields():
    """
    Test retrieving asset fields.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions

    with patch.object(
        limble_asset_field_interface, "get_asset_fields"
    ) as mock_get_asset_fields:
        mock_get_asset_fields.side_effect = [
            [
                {
                    "assetID": 1111,
                    "field": "Alim Part Number",
                    "value": "12345",
                    "fieldType": "Text",
                    "lastEdited": 1710948690,
                }
            ],
            [
                {
                    "assetID": 1111,
                    "field": "Alim Version",
                    "value": "1.0",
                    "fieldType": "Text",
                    "lastEdited": 1710948690,
                }
            ],
            [
                {
                    "assetID": 1111,
                    "field": "Serial Number",
                    "value": "SN12345",
                    "fieldType": "Text",
                    "lastEdited": 1710948690,
                }
            ],
            [
                {
                    "assetID": 1111,
                    "field": "Topological Name",
                    "value": "SOU+S08+FS345.FNDH+FNEP=FTIF",
                    "fieldType": "Text",
                    "lastEdited": 1710948690,
                }
            ],
        ]

        response = client.get("/limble/asset_fields")
        assert response.status_code == 200
        assert response.json() == [
            {
                "asset_id": 1111,
                "field": "Alim Part Number",
                "value": "12345",
                "type": "Text",
                "last_edited_date": 1710948690,
            },
            {
                "asset_id": 1111,
                "field": "Alim Version",
                "value": "1.0",
                "type": "Text",
                "last_edited_date": 1710948690,
            },
            {
                "asset_id": 1111,
                "field": "Serial Number",
                "value": "SN12345",
                "type": "Text",
                "last_edited_date": 1710948690,
            },
            {
                "asset_id": 1111,
                "field": "Topological Name",
                "value": "SOU+S08+FS345.FNDH+FNEP=FTIF",
                "type": "Text",
                "last_edited_date": 1710948690,
            },
        ]


def test_get_tasks():
    """
    Test retrieving work orders.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions

    with patch.object(limble_task_interface, "get_tasks") as mock_get_tasks:
        mock_get_tasks.return_value = [
            {
                "taskID": 1,
                "type": 7,
                "name": "Test Task",
                "description": "Test Description",
                "locationID": 10,
                "assetID": 100,
                "status": 0,
                "priority": 1,
                "startDate": 1723521600,
                "due": 1723708800,
                "createdDate": 1723000747,
                "dateCompleted": 0,
                "lastEdited": 1723000747,
            }
        ]

        response = client.get("/limble/tasks")
        assert response.status_code == 200
        assert response.json() == [
            {
                "task_id": 1,
                "type": 7,
                "name": "Test Task",
                "description": "Test Description",
                "location_id": 10,
                "asset_id": 100,
                "status": 0,
                "priority": 1,
                "start_date": 1723521600,
                "due_date": 1723708800,
                "created_date": 1723000747,
                "completed_date": 0,
                "last_edited_date": 1723000747,
            }
        ]


def test_create_task():
    """
    Test creating a work order.
    """
    app.dependency_overrides[require_write_permission] = mock_required_permissions
    with patch.object(limble_task_interface, "create_task") as mock_create_task:
        # Define the mock response for create_task
        mock_create_task.return_value = {
            "task_id": 1,
            "type": 7,
            "name": "Test Task",
            "description": "Test Description",
            "location_id": 10,
            "asset_id": 100,
            "status": 0,
            "priority": 1,
            "start_date": 1723521600,
            "due_date": 1723708800,
            "created_date": 1723000747,
            "completed_date": 0,
            "last_edited_date": 1723000747,
        }

        # Define the request payload
        work_order_payload = {
            "task_id": 1,
            "type": 7,
            "name": "Test Task",
            "description": "Test Description",
            "location_id": 10,
            "asset_id": 100,
            "status": 0,
            "priority": 1,
            "start_date": 1723521600,
            "due_date": 1723708800,
            "created_date": 1723000747,
            "completed_date": 0,
            "last_edited_date": 1723000747,
        }

        response = client.post("/limble/tasks", json=work_order_payload)
        assert response.status_code == 201
        assert response.json() == {
            "task_id": 1,
            "type": 7,
            "name": "Test Task",
            "description": "Test Description",
            "location_id": 10,
            "asset_id": 100,
            "status": 0,
            "priority": 1,
            "start_date": 1723521600,
            "due_date": 1723708800,
            "created_date": 1723000747,
            "completed_date": 0,
            "last_edited_date": 1723000747,
        }


def test_move_asset():
    """
    Test moving an asset.
    """
    app.dependency_overrides[require_write_permission] = mock_required_permissions
    with patch.object(limble_asset_interface, "get_assets") as mock_get_assets:
        with patch.object(
            limble_asset, "move_asset_with_children"
        ) as mock_move_asset_with_children:
            mock_get_assets.return_value = {
                "assetID": 1,
                "name": "Test Asset",
                "locationID": 10,
            }

            mock_move_asset_with_children.return_value = True

            response = client.patch("/limble/assets/1/move", params={"location_id": 20})
            assert response.status_code == 200
            assert response.json() == {"moved": True}


def test_move_asset_not_found():
    """
    Test moving a non-existent asset.
    """
    app.dependency_overrides[require_write_permission] = mock_required_permissions
    with patch.object(limble_asset_interface, "get_assets") as mock_get_assets:
        with patch.object(
            limble_asset, "move_asset_with_children"
        ) as mock_move_asset_with_children:
            mock_get_assets.return_value = None
            mock_move_asset_with_children.return_value = True

            response = client.patch("/limble/assets/1/move", params={"location_id": 20})
            assert response.status_code == 404
            assert response.json() == {"detail": "Asset does not exist"}


def test_remove_replace_asset():
    """
    Test remove and replace an asset.
    """
    app.dependency_overrides[require_write_permission] = mock_required_permissions

    with patch.object(
        limble_asset_interface, "get_assets"
    ) as mock_get_assets, patch.object(
        limble_asset, "create_placeholder_asset"
    ) as mock_create_placeholder_asset, patch.object(
        limble_asset, "move_asset_with_children"
    ) as mock_move_asset_with_children, patch.object(
        limble_asset_interface, "update_asset"
    ) as mock_update_asset, patch.object(
        limble_asset, "update_topological_names"
    ) as mock_update_topological_names:

        # Mock asset
        mock_get_assets.side_effect = [
            [
                {
                    "assetID": 1,
                    "name": "Test Asset",
                    "locationID": 20,
                    "parentAssetID": 2,
                }
            ],
            [{"assetID": 100, "name": "Test Spare", "locationID": 20}],
        ]

        # Placeholder
        mock_create_placeholder_asset.return_value = 100

        # Assume move_asset_with_children is always successful
        mock_move_asset_with_children.return_value = True

        # Perform patch request with required params
        response = client.patch("/limble/assets/1/replace", params={"location_id": 20})

        # Verify the response
        assert response.status_code == 201
        assert response.json() == {"moved": True}

        # Ensure all mock methods were called correctly
        mock_get_assets.assert_any_call(1)
        mock_get_assets.assert_any_call(100)
        mock_create_placeholder_asset.assert_called_once_with(1, 20)
        mock_move_asset_with_children.assert_any_call(1, 20)
        mock_update_asset.assert_any_call(100, parent_asset_id=2, name="Test Asset")
        mock_update_asset.assert_any_call(1, parent_asset_id=None, name="Test Spare")
        mock_update_topological_names.assert_called_once_with(1, 100)
