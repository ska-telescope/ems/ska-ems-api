"""
Test Technical Documentation endpoints
"""

from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_ems_api.main import app
from ska_ems_api.services.auth import require_read_permission
from ska_ems_api.services.confluence import api_interface

client = TestClient(app)


def mock_required_permissions():
    """
    Mock function to replace the permissions dependency
    """
    return None


def test_get_data_modules():
    """
    Test retrieving all datamodules.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions
    with patch.object(
        api_interface, "get_data_modules"
    ) as mock_get_data_modules, patch.object(
        api_interface, "read_page_content"
    ) as mock_read_page_content:
        mock_get_data_modules.return_value = {
            "results": [
                {
                    "id": "218410338",
                    "title": "AAVS-3-01-01-00-700 - AAVS-3 Noise Amplifier (LNA)",
                }
            ],
        }

        mock_read_page_content.return_value = {
            "title": "AAVS-3-01-01-00-700 - AAVS-3 Noise Amplifier (LNA) - Install",
            "body": {
                "storage": {
                    "value": """
                            <ac:layout>
                                <ac:layout-section ac:type="single">
                                    <ac:layout-cell>
                                        <table>
                                            <tr><td></td></tr>
                                        </table>
                                        <table
                                        style="width: 98.447%; letter-spacing: 0.0px;">
                                            <colgroup>
                                                <col style="width: 57.1202%;" /> <col
                                                style="width: 42.8798%;" />
                                            </colgroup> <tbody>
                                                <tr>
                                                    <td><strong>Issue no:
                                                    001</strong></td> <td><div
                                                    class="content-wrapper"><p><strong>Date:
                                                    </strong><time datetime="2023-03-31"
                                                    /></p></div></td>
                                                </tr> <tr>
                                                    <td><strong>Authority info:
                                                    <strong>SKA1-LOW <span
                                                    class="terms-mark
                                                    terms-term-selector">AA0.5</span>
                                                    USER MANUAL
                                                    (NP-2017-04-037)</strong></strong></td>
                                                    <td><strong>Applicability:</strong><strong>
                                                    CP07900</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ac:layout-cell>
                                </ac:layout-section>
                            </ac:layout>"""
                }
            },
        }

        response = client.get("/data-modules")
        assert response.status_code == 200
        assert response.json() == [
            {
                "confluence_id": 218410338,
                "name": "AAVS-3-01-01-00-700 - AAVS-3 Noise Amplifier (LNA)",
                "data_module_code": "AAVS-3-01-01-00-700",
                "issue_number": "001",
                "part_number": "CP07900",
                "url": (
                    "https://confluence.skatelescope.org/rle/working-version/"
                    "aavs-3-01-01-00-700-aavs-3-noise-amplifier-(lna)-218410338.html"
                ),
            }
        ]
