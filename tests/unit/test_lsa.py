"""
RamLog unit tests
"""

from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_ems_api.main import app

client = TestClient(app)

mock_data = [
    ("Ska-a", "P", "SKA OBSERVATORY", 1, 0, "000-000000", "SKA01", "SKA Observatory"),
]

expected_result = [
    {
        "system_name": "Ska-a",
        "lcn": "P",
        "lcn_name": "SKA OBSERVATORY",
        "qty_per_assembly": 1,
        "lru": False,
        "part_number": "000-000000",
        "cage_code": "SKA01",
        "cage_name": "SKA Observatory",
    }
]


def test_ramlog_successful_retrieval_of_lsa_product_structure():
    """
    Test the successful retrieval of product structure from Ramlog.
    """
    with patch(
        "ska_ems_api.services.ramlog.lsa_interface.get_product_breakdown_structure",
        return_value=mock_data,
    ):
        response = client.get("lsa/pbs")
        assert response.status_code == 200
        assert response.json() == expected_result
