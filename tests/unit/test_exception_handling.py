"""
Tests the handle_request_exceptions decorator.
"""

from unittest.mock import MagicMock, patch

import pytest
import requests
from fastapi import HTTPException

from ska_ems_api.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


def test_handle_timeout_exception():
    """
    Test the handle_request_exceptions decorator handles Timeout exceptions.
    """

    def mock_func():
        raise requests.exceptions.Timeout("Request timed out")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        decorated_func()

    assert exc_info.value.status_code == 504
    assert exc_info.value.detail == "Request timed out"


def test_handle_request_exceptions_connection_error():
    """
    Test the handle_request_exceptions decorator handles ConnectionError
    exceptions.
    """

    def mock_func():
        raise requests.exceptions.ConnectionError("Connection error occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        decorated_func()

    assert exc_info.value.status_code == 502
    assert exc_info.value.detail == "Connection error"


def test_handle_request_exceptions_http_error():
    """
    Test the handle_request_exceptions decorator handles HTTPError exceptions.
    """

    def mock_func():
        raise requests.exceptions.HTTPError()

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        decorated_func()

    assert exc_info.value.status_code == 404
    assert exc_info.value.detail == "HTTP error"


def test_handle_request_exceptions_request_exception():
    """
    Test the handle_request_exceptions decorator handles general
    RequestException.
    """

    def mock_func():
        raise requests.exceptions.RequestException("Request exception occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        decorated_func()

    assert exc_info.value.status_code == 500
    assert exc_info.value.detail == "Request exception"


@patch("ska_ems_api.utils.exception_handler.logging.error")
def test_handle_response_unauthorized(mock_logging_error):
    """
    Test the handle_response function handles 401 Unauthorized responses.
    """
    mock_response = MagicMock()
    mock_response.status_code = 401

    handle_response(mock_response)

    mock_logging_error.assert_called_once_with(
        "401 Unauthorized Error! Check your credentials."
    )


@patch("ska_ems_api.utils.exception_handler.logging.error")
def test_handle_response_other_error(mock_logging_error):
    """
    Test the handle_response function handles other errors and logs the
    response.
    """
    mock_response = MagicMock()
    mock_response.status_code = 400
    mock_response.json.return_value = {"error": "some error"}

    handle_response(mock_response)
    mock_logging_error.assert_called_with(
        "%s Status code: %s", "Unexpected response.", 400
    )


@patch("ska_ems_api.utils.exception_handler.logging.info")
def test_handle_response_success(mock_logging_info):
    """
    Test the handle_response function handles other errors and logs the
    response.
    """
    mock_response = MagicMock()
    mock_response.status_code = 200

    messages = {"success": "Successfully passed test (%s)"}
    handle_response(
        mock_response,
        messages,
        identifiers={
            "asset_id": "123",
            "field_id": "field_123",
            "location_id": "location_123",
            "task_id": "task_123",
            "name": "name_123",
        },
        return_json=True,
    )

    mock_logging_info.assert_called_with(
        "Successfully passed test (%s)",
        "field_123, 123, location_123, task_123, name_123",
    )
