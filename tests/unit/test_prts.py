"""
Test the Problem Reporting and Tracking System endpoints
"""

from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_ems_api.main import app
from ska_ems_api.models.shemas import TicketWorkOrder
from ska_ems_api.services.auth import require_read_permission, require_write_permission
from ska_ems_api.services.jira import jira_interface

client = TestClient(app)


def mock_required_permissions():
    """
    Mock function to replace the permissions dependency
    """
    return None


def test_get_tickets():
    """
    Test retrieving all tickets.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions
    with patch.object(jira_interface, "get_prts_tickets") as mock_get_prts_tickets:
        mock_get_prts_tickets.return_value = {
            "issues": [
                {
                    "id": "1",
                    "self": "https://jira.skatelescope.org/rest/api/2/issue/TICKET-1",
                    "key": "TICKET-1",
                    "fields": {
                        "summary": "Test Summary 1",
                        "description": "Test Description 1",
                        "components": [
                            {
                                "name": "LOW CSP",
                                "description": "102-000000 - Central Signal Processor",
                            }
                        ],
                        "status": {
                            "name": "Closed",
                        },
                        "resolution": {"name": "Resolved"},
                        "created": "2024-10-01T12:29:50.000+0000",
                        "resolutiondate": "2024-10-01T12:29:50.000+0000",
                    },
                },
                {
                    "id": "2",
                    "self": "https://jira.skatelescope.org/rest/api/2/issue/TICKET-2",
                    "key": "TICKET-2",
                    "fields": {
                        "summary": "Test Summary 2",
                        "description": "Test Description 2",
                        "components": [
                            {
                                "name": "LOW CSP",
                                "description": "102-000000 - Central Signal Processor",
                            }
                        ],
                        "status": {
                            "name": "Closed",
                        },
                        "resolution": {"name": "Resolved"},
                        "created": "2024-10-01T12:29:50.000+0000",
                        "resolutiondate": "2024-10-01T12:29:50.000+0000",
                    },
                },
            ]
        }

        response = client.get("/jira/tickets")
        assert response.status_code == 200
        assert response.json() == [
            {
                "id": 1,
                "key": "TICKET-1",
                "summary": "Test Summary 1",
                "description": "Test Description 1",
                "components": [
                    {
                        "part_number": "102-000000",
                        "name": "Central Signal Processor",
                    }
                ],
                "status": "Closed",
                "resolution": "Resolved",
                "created_date": 1727785790,
                "resolution_date": 1727785790,
                "url": "https://jira.skatelescope.org/browse/TICKET-1",
            },
            {
                "id": 2,
                "key": "TICKET-2",
                "summary": "Test Summary 2",
                "description": "Test Description 2",
                "components": [
                    {
                        "part_number": "102-000000",
                        "name": "Central Signal Processor",
                    }
                ],
                "status": "Closed",
                "resolution": "Resolved",
                "created_date": 1727785790,
                "resolution_date": 1727785790,
                "url": "https://jira.skatelescope.org/browse/TICKET-2",
            },
        ]


def test_get_ticket_work_orders():
    """
    Test retrieving work orders linked to a JIRA ticket.
    """
    app.dependency_overrides[require_read_permission] = mock_required_permissions
    with patch.object(
        jira_interface, "get_ticket_work_order"
    ) as mock_get_ticket_work_order:
        mock_get_ticket_work_order.return_value = [
            {
                "id": 1,
                "object": {
                    "url": "http://example.com",
                    "title": "Work Order 1",
                    "summary": "Summary 1",
                    "status": {"resolved": True},
                },
            }
        ]

        response = client.get("/jira/tickets/TICKET-1/work-orders")
        assert response.status_code == 200
        assert response.json() == [
            {
                "id": 1,
                "url": "http://example.com",
                "title": "Work Order 1",
                "summary": "Summary 1",
                "resolved": True,
            }
        ]


def test_update_ticket_work_order():
    """
    Test updating a work order linked to a JIRA ticket.
    """
    app.dependency_overrides[require_write_permission] = mock_required_permissions
    with patch.object(
        jira_interface, "update_ticket_work_order"
    ) as mock_update_ticket_work_order:
        mock_update_ticket_work_order.return_value = None
        ticket_workorder_payload = TicketWorkOrder(
            title="Updated Work Order",
            url="http://example.com",
            summary="Updated Summary",
            resolved="true",
        )

        response = client.put(
            "/jira/tickets/TICKET-1/work-order", json=ticket_workorder_payload.dict()
        )
        assert response.status_code == 204


def test_create_ticket_work_order():
    """
    Test creating a work order linked to a JIRA ticket.
    """
    app.dependency_overrides[require_write_permission] = mock_required_permissions
    with (
        patch.object(
            jira_interface, "create_ticket_work_order"
        ) as mock_create_ticket_work_order,
        patch(
            "ska_ems_api.controllers.prts.create_issue_work_order_link"
        ) as mock_create_issue_work_order_link,
    ):
        mock_create_ticket_work_order.return_value = {"id": "remoteLinkId"}
        mock_create_issue_work_order_link.return_value = {"id": "remoteLinkId"}

        ticket_workorder_payload = TicketWorkOrder(
            title="Work Order #123",
            url="http://example.com",
            summary="Updated Summary",
            resolved="true",
        )

        response = client.post(
            "/jira/tickets/TICKET-1/work-order", json=ticket_workorder_payload.dict()
        )
        assert response.status_code == 201
        assert response.json() == {"jira_remote_link_id": "remoteLinkId"}


# def test_delete_ticket_work_order():
#     """
#     Test deleting a work order linked to a JIRA ticket.
#     """
#     app.dependency_overrides[require_write_permission] = mock_required_permissions
#     with (
#         patch.object(
#             jira_interface, "delete_ticket_work_order"
#         ) as mock_delete_ticket_work_order,
#         patch(
#             "ska_ems_api.controllers.prts.delete_issue_work_order_link"
#         ) as mock_delete_issue_work_order_link,
#     ):
#         mock_delete_ticket_work_order.return_value = None
#         mock_delete_issue_work_order_link.return_value = None
#         delete_payload = {"id": "remoteLinkId"}

#         response = client.delete(
#             url="/jira/tickets/TICKET-1/work-order",
#             json=delete_payload,
#         )
#         assert response.status_code == 204
