"""
Test the asset functions for Limble PI
"""

from unittest.mock import MagicMock, patch

from ska_ems_api.services.cmms.limble_asset import (
    get_all_child_assets,
    get_field_values,
    move_asset_with_children,
    update_asset_fields,
)


def test_move_asset_with_children():
    """
    Test the move_asset_with_children function to ensure it correctly moves an asset
    and all its child assets to a new location.
    """
    with patch(
        "ska_ems_api.services.cmms.limble_asset.limble_asset_interface"
    ) as mock_asset_interface, patch(
        "ska_ems_api.services.cmms.limble_asset.get_all_child_assets"
    ) as mock_get_all_child_assets:
        mock_get_all_child_assets.return_value = [
            {"assetID": 1, "parentAssetID": 0},
            {"assetID": 2, "parentAssetID": 1},
            {"assetID": 3, "parentAssetID": 1},
        ]
        mock_asset_interface.move_asset = MagicMock()
        mock_asset_interface.update_asset = MagicMock()

        result = move_asset_with_children(asset_id=1, location_id=100)

        mock_get_all_child_assets.assert_called_once_with(1)

        # Check that move_asset was called for each child asset
        mock_asset_interface.move_asset.assert_any_call(3, 100)
        mock_asset_interface.move_asset.assert_any_call(2, 100)
        mock_asset_interface.move_asset.assert_any_call(1, 100)

        mock_asset_interface.update_asset.assert_any_call(1, 0)
        mock_asset_interface.update_asset.assert_any_call(2, 1)
        mock_asset_interface.update_asset.assert_any_call(3, 1)

        assert result


def test_get_all_child_assets():
    """
    Test the get_all_child_assets function to ensure it correctly retrieves an
    asset and all its child assets recursively.
    """
    with patch(
        "ska_ems_api.services.cmms.limble_asset.limble_asset_interface"
    ) as mock_get_assets, patch(
        "ska_ems_api.services.cmms.limble_asset.limble_asset_interface"
        ".get_asset_children"
    ) as mock_get_asset_children:
        mock_get_assets.get_assets.return_value = [
            {"assetID": 1, "name": "Parent Asset"}
        ]

        mock_get_asset_children.side_effect = [
            [
                {"assetID": 2, "name": "Child Asset 1"},
                {"assetID": 3, "name": "Child Asset 2"},
            ],
            [{"assetID": 4, "name": "Child Asset 3"}],
            [],
            [],
        ]

        result = get_all_child_assets(asset_id=1)

        expected_result = [
            {"assetID": 1, "name": "Parent Asset"},
            {"assetID": 2, "name": "Child Asset 1"},
            {"assetID": 4, "name": "Child Asset 3"},
            {"assetID": 3, "name": "Child Asset 2"},
        ]
        assert result == expected_result


def test_update_asset_fields():
    """
    Test the update_asset_fields function to ensure it correctly updates fields
    of an asset with the provided field values.
    """
    with patch(
        "ska_ems_api.services.cmms.limble_asset.get_field_values"
    ) as mock_get_field_values, patch(
        "ska_ems_api.services.cmms.limble_asset_field_interface"
        ".update_asset_field_value"
    ) as mock_update_asset_field:

        # Mock the return value of get_field_values to simulate current field values
        mock_get_field_values.return_value = {
            "name": {"valueID": 101, "value": "Old Asset"},
            "location": {"valueID": 102, "value": "Old Location"},
        }

        # Define field values to be updated
        field_values = {"name": "Updated Asset", "location": "New Location"}

        # Call the function with test data
        update_asset_fields(asset_id=1, field_values=field_values)

        # Verify that update_asset_field_value was called with the correct arguments
        mock_update_asset_field.assert_any_call(101, "Updated Asset")
        mock_update_asset_field.assert_any_call(102, "New Location")

        # Ensure get_field_values is called once with the correct arguments
        mock_get_field_values.assert_called_once_with(1, {"name", "location"})


def test_get_field_values():
    """
    Test the get_field_values function to ensure it correctly retrieves fields
    of an asset with the provided field values.
    """
    with patch(
        "ska_ems_api.services.cmms.limble_asset.limble_asset_field_interface"
        ".get_asset_fields"
    ) as mock_get_asset_fields:

        mock_get_asset_fields.return_value = [
            {"field": "name", "valueID": 101, "value": "value1"},
            {"field": "location", "valueID": 102, "value": "value2"},
            {"field": "part number", "valueID": 103, "value": "value3"},
        ]

        # Define the input parameters for the test
        field_names = {"name", "location"}

        # Call the function with test data
        result = get_field_values(asset_id=123, field_names=field_names)

        expected_result = {
            "name": {"valueID": 101, "value": "value1"},
            "location": {"valueID": 102, "value": "value2"},
        }

        assert result == expected_result
