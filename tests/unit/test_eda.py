"""
EDA unit tests
"""

from datetime import datetime
from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_ems_api.main import app

client = TestClient(app)

mock_data = [
    ("name1", 10.5, datetime(2023, 1, 1, 11, 0, 0)),
    ("name1", 11, datetime(2023, 1, 1, 12, 0, 0)),
    ("name2", 20.1, datetime(2023, 1, 1, 12, 0, 0)),
]


expected_result = [
    {
        "name": "name1",
        "value": "10.5",
        "date": 1672570800.0,
    },
    {
        "name": "name1",
        "value": "11",
        "date": 1672574400.0,
    },
    {
        "name": "name2",
        "value": "20.1",
        "date": 1672574400.0,
    },
]


@patch("ska_ems_api.controllers.eda.get_att_current_values")
def test_eda_successful_get_current_value(mock_get_current_values):
    """
    Test the successful retrieval of the current value from a component from the EDA.
    """
    mock_get_current_values.return_value = [
        ("name1", 10.5, datetime(2023, 1, 1, 12, 0, 0)),
        ("name2", 20.1, datetime(2023, 1, 1, 12, 0, 0)),
    ]

    data = ["name1", "name2"]
    response = client.post("/eda/value", json=data)

    assert response.status_code == 200
    assert response.json() == [
        {
            "name": "name1",
            "value": "10.5",
            "date": 1672574400.0,
        },
        {
            "name": "name2",
            "value": "20.1",
            "date": 1672574400.0,
        },
    ]


@patch("ska_ems_api.controllers.eda.get_att_value_ranges")
def test_eda_successful_get_value_range(mock_get_value_range):
    """
    Test the successful retrieval of a range of values for a component from the EDA.
    """
    mock_get_value_range.return_value = mock_data

    data = ["name1", "name2"]
    response = client.post(
        "eda/value-range?start_date=2023-01-01&end_date=2023-01-02", json=data
    )

    assert response.status_code == 200
    assert response.json() == expected_result
