Engineering Management System (EMS) Api Gateway
===============================================

The EMS API Gateway facilitates secure and efficient data exchange between the Engineering Management System (EMS) and external systems, ensuring consistent data synchronization.
It supports modular development with a microservices approach, providing a unified interface to streamline operations and enhance data accuracy.

.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Table of Contents
  
  error_codes
  rest_api.rst

